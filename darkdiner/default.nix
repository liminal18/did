{ darkdiner ? { outPath = ./.; name = "darkdiner"; }
, pkgs ? import <nixpkgs> {}
}:
let
  nodePackages = import "${pkgs.path}/pkgs/top-level/node-packages.nix" {
    inherit pkgs;
    inherit (pkgs) stdenv nodejs fetchurl fetchgit;
    neededNatives = [ pkgs.python ] ++ pkgs.lib.optional pkgs.stdenv.isLinux pkgs.utillinux;
    self = nodePackages;
    generated = ./package.nix;
  };
in rec {
  tarball = pkgs.runCommand "darkdiner-1.0.0.tgz" { buildInputs = [ pkgs.nodejs ]; } ''
    mv `HOME=$PWD npm pack ${darkdiner}` $out
  '';
  build = nodePackages.buildNodePackage {
    name = "darkdiner-1.0.0";
    src = [ tarball ];
    buildInputs = nodePackages.nativeDeps."darkdiner" or [];
    deps = [ nodePackages.by-spec."babel"."^6.3.26" nodePackages.by-spec."babel-preset-es2015"."^6.3.13" nodePackages.by-spec."babel-preset-react"."^6.3.13" nodePackages.by-spec."bootstrap"."^3.3.6" nodePackages.by-spec."css-loader"."^0.23.1" nodePackages.by-spec."extract-text-webpack-plugin"."^1.0.1" nodePackages.by-spec."file-loader"."^0.8.5" nodePackages.by-spec."jquery"."^2.2.0" nodePackages.by-spec."jquery-ui"."^1.12.0" nodePackages.by-spec."postcss-loader"."^0.9.1" nodePackages.by-spec."react"."^15.3.0" nodePackages.by-spec."react-addons-css-transition-group"."^15.0.2" nodePackages.by-spec."react-dom"."^15.3.0" nodePackages.by-spec."react-image-holder"."^2.0.1" nodePackages.by-spec."react-scrollbar"."^0.4.1" nodePackages.by-spec."redux"."^3.0.6" nodePackages.by-spec."sass-loader"."^3.2.0" nodePackages.by-spec."style-loader"."^0.13.1" nodePackages.by-spec."toolbox-loader"."0.0.3" ];
    peerDependencies = [];
  };
}