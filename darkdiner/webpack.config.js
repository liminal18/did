const webpack = require("webpack");
const path = require('path');
/***
* Return true if we are in production
*/
function production (p) {
  console.log('called production');
  if(!p){
    console.log('no process');
    return false;
  } 
  if(!p.env){
    console.log('no process env');
    return false;
  }
  if(!p.env.NODE_ENV){
    console.log('no node env var set');
    return false;
  }
  console.log(p.env.NODE_ENV);
  return p.env.NODE_ENV.toLowerCase() === 'production' || false 
};


module.exports = (env) => ({
  entry: ['./src/index.js'],
  output: { filename: 'dist.js'},
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        loaders: ['babel-loader']
      },
      {
        test:/\.scss$/,
        loaders: [ 
          'style-loader', 
          { loader: 'css-loader', 
            options: {url: false, minimize: production(process)}
          }, 
          'sass-loader']
      },
      {
        test:/\.(eot|svg|ttf|woff|woff2)$/,
        loaders:['file-loader']
      }
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      "$":"jquery",
      "jQuery":"jquery",
      "window.jQuery":"jquery"
    }),
    new webpack.EnvironmentPlugin({NODE_ENV: process.env.NODE_ENV || 'production'}) 
  ],
  resolve: {
    alias: {
      Components: path.resolve(__dirname, 'src/components/'),
      Services: path.resolve(__dirname, 'src/Services/'),
      Redux: path.resolve(__dirname, 'src/redux/'),
      Style: path.resolve(__dirname, 'src/style/')
    },
    extensions: ['.js', '.jsx']
  } 
});
