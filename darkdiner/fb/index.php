<?PHP

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors',1);
error_reporting(-1);

require('private/private.php');

  if(!array_key_exists("action", $_GET))
  {
    echo "no action";
    die();
  }

  if(!array_key_exists("loc", $_GET))
  {
    echo "no location";
    die();
  }


  $action = $_GET["action"];
  $location = $_GET["loc"];
  $id = $_GET["id"];

  $apiQUERY = "/";

  if($location == 1)
  {
     $apiQUERY .= $fb_bangkok['id'];
  }


  if($location == 2)
  {
    $apiQUERY .= $fb_phnomPenh['id'];
  }

  if($action ==  "album" && !$id)
  {
        $apiQUERY .= "/albums";
  }
  if($action == "album" && $id)
  {
    $apiQUERY =  $id . "/photos?fields=id,name,images,updated_time,album,likes";
  }


try {
header("HTTP/1.0 200 OK");
header("content-type: application/json");
header("Access-Control-Allow-Origin: *");
$page_result = $fb->api($apiQUERY);
if(!$id){
  list_names($page_result);
}
else{
  echo json_encode($page_result["data"]);
}

} catch (Exception $e){
  var_dump($e);
}

function return_data($result, $id)
{
  foreach ($result["data"] as $value) {
    if($value['id'] == $id)
    {
      return  $value;
    }
  }
}

function list_names($page_result)
{
  echo json_encode($page_result["data"]);

}

?>
