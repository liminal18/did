module.exports = {
    "extends": "airbnb",
    "plugins": [
        "react",
        "jsx-a11y",
        "import"
    ],
    "rules": {
     "linebreak-style": "off",
     "func-names": "off"
     }
};
