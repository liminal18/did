<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Reservation;
use AppBundle\Entity\Contact;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
    * @Route("/contact", name="contact")
    * @Method({"POST"})
    */
    public function contactAction(Request $request)
    {
      $location = $request->get('loc');
      $email = $this->container->getParameter($location == 1 ? 'bkkEmail':'cambodiaEmail');
      $contact = new Contact($request->headers->get('Content-Type'), $request->GetContent());
      $validator = $this->get('validator');
      $errors = $validator->validate($contact);
      if(count($errors) > 0) {
        $errorArray = array();
        foreach($errors as $error)
        {
          $errorArray[$error->getPropertyPath()] = $error->getMessage();
        }
        return new JsonResponse($errorArray, 422);
      }
      $html = $this->renderView('contact.html.twig', array('form' => $contact));
      $message = $contact->toEmail($email, $html);
      $this->get('mailer')->send($message);  
      return new JsonResponse('Valid Contact', 200);
    }

    /**
    * @Route("/reservation", name="reservation")
    * @Method({"POST"})
    */
    public function reservationAction(Request $request)
    {
      $location = $request->get('loc');
      $email = $this->container->getParameter($location == 1 ? 'bkkEmail':'cambodiaEmail');
      $reservation = 
        new Reservation(
          $request->headers->get('Content-Type'), 
          $request->GetContent()
        ); 
      $validator = $this->get('validator');
      $errors = $validator->validate($reservation);
      if(count($errors) > 0) {
        $errorArray = array();
        foreach($errors as $error)
        {
          $errorArray[$error->getPropertyPath()] = $error->getMessage();
        }
        return new JsonResponse($errorArray, 422);
      }
      $html = $this->renderView('reservation.html.twig', array('form' => $reservation));
      $message = $reservation->toEmail($email, $html);
      $this->get('mailer')->send($message);
      return new JsonResponse('Valid Reservation', 200);
    }

    /**
    * @Route("/facebook", name="facebook"))
    * @Method({"GET"})
    */
    public function facebookAction(Request $request)
    {
      $app_id = $this->container->getParameter('facebookAppId');
      $app_secret = $this->container->getParameter('facebookSecret');
      $access_token = $this->container->getParameter('facebookAccessToken');
 
      $fb = new \Facebook\Facebook([
        'app_id' => $app_id,
        'app_secret' => $app_secret,
        'default_access_token' => $access_token,
        'default_graph_version' => 'v2.9',
      ]);
      try {
        $query = "/";
        $action = $request->get('action');
        $location = $request->get('loc');
        $id = $request->get('id');
        $fb_phnomPenh = array("id" => 585026824867197, "name" => "DIDPhnomPenh");
        $fb_bangkok = array("id"=> 313484745335868, "name" => "DIDBangkok");
        // location 1 is Bangkok
        if($location == 1){
          $query .= $fb_bangkok['id'];
        }
        // location 2 is Phnom Penh
        if($location == 2){
          $query .= $fb_phnomPenh['id'];
        }
        // if it's bigger than2 then we don't support it yet
        if($location > 2){
          return new JsonResponse('invalid location', 422);
        }
        // there is only one action right now but the fb api is big better to stay open
        if(!$action){
          return new JsonResponse('invalid action', 422);
        }
        // if there is no album specified get all albums
        if(!$id && $action === "album"){
          $query .= "/albums";
        }
        // if there is an id and we want an album then fetch that album
        if($action == "album" && $id)
        {
          $query =  $id . "/photos?fields=id,name,images,updated_time,album,likes";
        }

        $result = $fb->get($query);
        $data = json_encode($result->getDecodedBody());
        return new JsonResponse($data);
      } catch(Facebook\Exceptions\FacebookResponseException $e) {
          echo 'Graph returned an error: ' . $e->getMessage();
          return new JsonResponse('facebook Response Error');
      } catch(Facebook\Exceptions\FacebookSDKException $e) {
         echo 'Facebook SDK returned an error: ' . $e->getMessage();
         return new JsonResponse('facebook SDK Error');
      } 
    }

    /**
    * @Route("/google", name="google"))
    * @Method({"GET"})
    */
    public function googleAction(Request $request)
    {
      $gmapKey = $this->container->getParameter('googleMaps');
      return new JsonResponse($gmapKey, 200);
    }

}
