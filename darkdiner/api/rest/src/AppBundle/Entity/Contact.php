<?php
// src/AppBundle/Entity/Contact.php
namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Swiftmailer;

class Contact
{
    public function __construct($type, $content){
      //sanitize the string
      $type = strtolower(trim($type));
      // if it's json
      if(strpos($type, 'json') !== false){
        $json = json_decode($content, true);
        foreach($json as $key => $value){
          $this->$key = $value;
        } 
      } else {

      }
    }

    /**
    *  @Assert\NotBlank()
    *  @Assert\Length(min=3)
    */
    public $name;
 
    /**
    *  @Assert\NotBlank()
    *  @Assert\Email(
    *      message = "The email '{{ value }}' is not a valid email.",
    *      checkMX = true,
    *      checkHost = true
    *   )
    */
    public $email;

    public $phone;

    public $messsage;

    public function toEmail($to, $body){ 
       $message = \Swift_Message::newInstance();
       $message->setSubject('New Contact from ' . $this->email);
       $message->setFrom($to);
       $message->setReplyTo(array($this->email));
       $message->setTo([$to]);
       $message->setContentType('text/html');
       $message->setBody($body, 'text/html');
       return $message;
    } 
}


