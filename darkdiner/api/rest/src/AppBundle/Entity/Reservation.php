<?php
// src/AppBundle/Entity/Reservation.php
namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

class Reservation
{
    public function __construct($type, $content){
      //sanitize the string
      $type = strtolower(trim($type));
      // if it's json
      if(strpos($type, 'json') !== false){
        $json = json_decode($content, true);
        foreach($json as $key => $value){
          $this->$key = $value; 
        } 
      } else {

      }
    }

    /**
    *  @Assert\NotBlank()
    */
    public $FullName;
 
    /**
    *  @Assert\NotBlank()
    *  @Assert\Email(
    *      message = "The email '{{ value }}' is not a valid email.",
    *      checkMX = true,
    *      checkHost = true
    *   )
    */
    public $EmailReservation;

    /**
    *  @Assert\NotBlank()
    */
    public $PhoneReservation;

    /**
    *  @Assert\NotBlank()
    */
    public $Calendar;

    /**
    *  @Assert\NotBlank()
    */
    public $Time;

    /**
    *  @Assert\NotBlank()
    *  @Assert\Range(
    *      min = 1,
    *      max = 20,
    *      minMessage = "You must have at least {{ limit }} guest",
    *      maxMessage = "For groups larger than {{ limit }} please call DiD"
    *   )
    */
    public $Group;

    public $Notes;

    public function toEmail($to, $html){ 
       $message = \Swift_Message::newInstance();
       $message->setSubject('DID - Dine in the Dark Reservation for ' . $this->EmailReservation);
       $message->setFrom($to);
       $message->setReplyTo(array($this->EmailReservation));
       $message->setTo([$to]);
       $message->setContentType('text/html');
       $message->setBody($html, 'text/html');
       return $message;
    } 
}


