<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{

    private function sendJson($uri, $json, $method){
       $client = static::createClient();
       $request = $client->request(
         $method,
         $uri,
         array(),
         array(),
         array('CONTENT_TYPE' => 'application/json'),
         $json
       );
 
       return $client;
    }

    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Welcome to Symfony', $crawler->filter('#container h1')->text());
    }
    
    public function testFacebook()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/facebook?action=album&loc=1');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }


   public function testContact(){
     $contactForm = '{
       "name": "tester",
       "email": "aljones15@gmail.com",
       "phone": "6681871733"
     }';
     $client = $this->sendJson('/contact', $contactForm, 'POST');
     $this->assertEquals(200, $client->getResponse()->getStatusCode());

   }

  public function testReservation(){
    $reservationForm = '{
      "FullName": "Test User",
      "EmailReservation": "aljones15@gmail.com",
      "PhoneReservation": "3212312",
      "Calendar": "12/04/2017",
      "Time": "18:30",
      "Group": 2,
      "Notes": "Something"
    }';
   $client = $this->sendJson('/reservation', $reservationForm, 'POST');
   $this->assertEquals(200, $client->getResponse()->getStatusCode());
  }
}
