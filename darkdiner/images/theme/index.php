<?php
/**
 * Template Name: home
 */

get_header(); 

// get background
$bg = array();
$args = array( 'post_type' => 'background');
$loop = new WP_Query($args);
while ( $loop->have_posts() ) : 
    $loop->the_post();
    $bg[] = get_the_ID();
endwhile;
?>

<div id="wrapper">
    <div class="container">
        <div class="row">
            <div class="col col-3">
                <div id="sidebar">
                    <h1><a href="index.html" id="logo"><img src="images/logo.png" alt=""></a></h1>
                    <ul id="mainnav" class="sidebar-nav">
                        <li class="active"><a href="#" data-page-id="0">Home</a></li>
                        <li><a href="#" data-page-id="1">Experience</a></li>
                        <li><a href="#" data-page-id="2">Cuisine</a></li>
                        <li><a href="#" data-page-id="3">Social Purpose</a></li>
                        <li><a href="#" data-page-id="4">Events</a></li>
                        <li><a href="#" data-page-id="5">Media</a></li>
                        <li><a href="#" data-page-id="6">Press</a></li>
                        <li><a href="#" data-page-id="7">Reservations</a></li>
                        <li><a href="#" data-page-id="8">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col col-9">
                <div id="main">
                    <div class="jcarousel">
                    <ul  id="page-carousel">
                        <li data-page-id="0">
                            <div id="breadcrumb">Home</div>
                            <div class="page-content">
                                <div class="scroll-pane">
                                    <p>Dine in the Dark (DID) is a fine dining restaurant with a unique concept. Guests dine in a dining room in complete darkness. The dinner is a real sensory experience, where all non-visual senses are exacerbated. It is also an interesting, fun and enjoyable experience. The concept and venue is sophisticated, yet the feel is relaxed and casual.
                                    </p>
                                    <p>
                                        The restaurant has a social purpose, as the welcoming team of waiters is all made of qualified young   university students. Dine in the Dark thus provides them with an employment opportunity and gives guests the opportunity to interact with them in a unique context. Moreover, 10% of the net profits made by Dine in the Dark are going to skills development projects for the visually impaired.
                                    </p>
                                    <p>
                                        There is a variety of (currently 3) different surprise menus: Thai, International and Vegetarian. Dine in the Dark also accommodates guests with specific dietary restrictions and prepares the dishes accordingly. Guests choose the type of menu they like, and the discovery of the dishes are a surprise.
                                    </p>
                                    <h2>Open every day</h2>
                                    <p>Seating time: 7:30 to 8:30 pm.</p>
                                    <p>Open until 11:00 pm.</p>
                                    <div class="row-img">
                                        <a class="lb-image-link" href="images/lightbox/image-1.jpg" data-lightbox="lb-1"><img src="images/lightbox/thumb-1.jpg" alt="thumb-1" width="185" height="118"/></a>
                                        <a class="lb-image-link" href="images/lightbox/image-2.jpg" data-lightbox="lb-2"><img  src="images/lightbox/thumb-2.jpg" alt="thumb-2" width="185" height="118"/></a>
                                        <a class="lb-image-link" href="images/lightbox/image-3.jpg" data-lightbox="lb-3"><img src="images/lightbox/thumb-3.jpg" alt="thumb-3" width="185" height="118"/></a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li data-page-id="1">
                            <div id="breadcrumb">Experience</div>
                            <div class="page-content">
                                <div class="scroll-pane">
                                    <h2 class="title">The Concept</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                                    <h2 class="title">The Experience</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                    <h2 class="title">The Process</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>

                                    <div class="row-img">
                                        <a class="lb-image-link" href="images/lightbox/image-1.jpg" data-lightbox="lb-1"><img src="images/lightbox/thumb-1.jpg" alt="thumb-1" width="185" height="118"/></a>
                                        <a class="lb-image-link" href="images/lightbox/image-2.jpg" data-lightbox="lb-2"><img  src="images/lightbox/thumb-2.jpg" alt="thumb-2" width="185" height="118"/></a>
                                        <a class="lb-image-link" href="images/lightbox/image-3.jpg" data-lightbox="lb-3"><img src="images/lightbox/thumb-3.jpg" alt="thumb-3" width="185" height="118"/></a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li data-page-id="2">
                            <div id="breadcrumb">Cuisine</div>
                            <div class="page-content">
                                <div class="scroll-pane">
                                    <p>The most fun things in life are done in the dark, like playing football in the woods at midnight, putting on makeup with the bathroom lights off and the last we can't mention without blushing. Get your mind out of the gutter with today's Groupon for Dine in the Dark. Choose from the following three options :</p>
                                    
                                    <ul class="disc">
                                        <li>
                                            Pay ฿549 instead of ฿1,000 for a 3 Course Menu for 1 Person (45% off)
                                        </li>
                                        <li>
                                             Pay ฿1,079 instead of ฿2,000 for a 3 Course Menu for 2 People (46% off)
                                        </li>
                                        <li>
                                            Pay ฿2,119 instead of ฿4,000 for a 3 Course Menu for 4 People (47% off)
                                        </li>
                                    </ul>

                                    <h2 class="title">Highlights</h2>
                                    <ul class="disc">
                                        <li>Exclusive offer on the occasion of Groupon’s grand opening.</li>
                                        <li>First ever premiere dark dining concept restaurant in Bangkok, providing a culinary experience that heightens non-visual senses</li>
                                        <li>Features fine Thai, international and vegetarian surprise menus</li>
                                        <li>Concept started in Europe, North America and is now spreading all across Asia</li>
                                        <li>Has social purpose of supporting the visually impaired</li>
                                        <li>Located on the 2nd floor of Ascott Sathorn Bangkok</li>
                                    </ul>
                                    
                                    <h2 class="title">3 Cuisine</h2>

                                    <div class="row-img">
                                        <a class="lb-image-link" href="images/lightbox/image-4.jpg" data-lightbox="lb-4"><img src="images/lightbox/thumb-4.jpg" alt="thumb-4" width="185" height="118"/></a>
                                        <a class="lb-image-link" href="images/lightbox/image-5.jpg" data-lightbox="lb-5"><img  src="images/lightbox/thumb-5.jpg" alt="thumb-5" width="185" height="118"/></a>
                                        <a class="lb-image-link" href="images/lightbox/image-6.jpg" data-lightbox="lb-6"><img src="images/lightbox/thumb-6.jpg" alt="thumb-6" width="185" height="118"/></a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li data-page-id="3">
                            <div id="breadcrumb">Social Purpose</div>
                            <div class="page-content">
                                <div class="scroll-pane">
                                    <h2 class="title">Social purpose</h2>
                                    <p>DID - Dine in the Dark has a social purpose embedded at the core of its activities:</p>

                                    <ol>
                                        <li>
                                            It supports the principles of equal employment opportunities, as all members of its service team are talented bilingual visually impaired waiters.
                                        </li>
                                        <li>
                                            The restaurant aims at having a lasting social impact and allocates 10% of its net profits to professional skills development programs in support of the visually impaired in Thailand.
                                        </li>
                                        <li>
                                            It provides guests with an enlightening opportunity to take on the perspective of the visually impaired.
                                        </li>
                                    </ol>
                                </div>
                            </div>
                        </li>
                        <li data-page-id="4">
                            <div id="breadcrumb">Events</div>
                            <div class="page-content">
                                <div class="scroll-pane">
                                    <!-- <div class="event-content">
                                        <h2 class="title">Events in the 3 month.</h2>
                                        <div class="event-img">
                                            <img src="images/lightbox/thumb-1.jpg" alt="">
                                        </div>
                                        <div class="event-detail">
                                            <div class="event-date">November 17, 2013 6:00 PM</div>
                                            <p>aaaaaaaaaaaaaa</p>
                                        </div>
                                    </div> -->
                                    <h2 class="title">Events in the 3 month.</h2>
                                    <div class="event-content">
                                    <div class="row">
                                        <div class="col col-4">
                                            <img class="event-img" src="images/lightbox/thumb-1.jpg" alt="">
                                        </div>
                                        <div class="col col-8">
                                            <div class="event-detail">
                                                <div class="event-date">November 17, 2013 6:00 PM</div>
                                                <h3>Loy Kratong event</h3>
                                                <p>The restaurant has a social purpose, as the welcoming team of waiters is all made of qualified young   university students. Dine in the Dark thus provides them with an employment opportunity and gives guests the opportunity to interact with them in a unique context. Moreover, 10% of the net profits made by Dine in the Dark are going to skills development projects for the visually impaired.</p>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="event-content">
                                    <div class="row">
                                        <div class="col col-4">
                                            <img class="event-img" src="images/lightbox/thumb-2.jpg" alt="">
                                        </div>
                                        <div class="col col-8">
                                            <div class="event-detail">
                                                <div class="event-date">December 25, 2013 10:00 AM</div>
                                                <h3>Christmas</h3>
                                                <p>The restaurant has a social purpose, as the welcoming team of waiters is all made of qualified young   university students. Dine in the Dark thus provides them with an employment opportunity and gives guests the opportunity to interact with them in a unique context. Moreover.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="event-content">
                                    <div class="row">
                                        <div class="col col-4">
                                            <img class="event-img" src="images/lightbox/thumb-3.jpg" alt="">
                                        </div>
                                        <div class="col col-8">
                                            <div class="event-detail">
                                                <div class="event-date">December 31, 2013 8:30 AM</div>
                                                <h3>Year End Party</h3>
                                                <p>Dine in the Dark (DID) is a fine dining restaurant with a unique concept. Guests dine in a dining room in complete darkness. The dinner is a real sensory experience, where all non-visual senses are exacerbated. It is also an interesting, fun and enjoyable experience.</p>
                                                <p>The restaurant has a social purpose, as the welcoming team of waiters </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </li>
                        <li data-page-id="5">
                            <div id="breadcrumb">Media</div>
                            <div class="page-content">
                                <div class="scroll-pane">
                                    <h2 class="title">Din in the dark / bangkok (short edit)</h2>
                                    <div class="row">
                                        <div class="col col-9">
                                            <div id="video" class="player">
                                                      <iframe width="465" height="255" src="https://www.youtube.com/embed/NL7iLFnt_Xg" frameborder="0" allowfullscreen></iframe>
                                              </div>
                                              <p>DID - Dine in the Dark also has a social purpose. The restaurant employs qualified bilingual visually impaired individuals and provides them with employment opportunities. Moreover.</p>
                                        </div>
                                        <div class="col col-3">
                                            <div class="media-box">
                                                <div id="vslide">
                                                    <a class="buttons prev" href="#">left</a>
                                                    <div class="viewport">
                                                        <ul class="overview">
                                                            <li>
                                                                <div class="vdothumb"> <a href="#" rel="https://www.youtube.com/embed/NL7iLFnt_Xg" class="active"><img src="images/video/video1.jpg"></a> </div>
                                                            </li>
                                                            <li>
                                                                <div class="vdothumb"> <a href="#" rel="https://www.youtube.com/embed/NL7iLFnt_Xg" class="active"><img src="images/video/video2.jpg"></a> </div>
                                                            </li>
                                                            <li>
                                                                <div class="vdothumb"> <a href="#" rel="https://www.youtube.com/embed/NL7iLFnt_Xg" class="active"><img src="images/video/video3.jpg"></a> </div>
                                                            </li>
                                                            <li>
                                                                <div class="vdothumb"> <a href="#" rel="https://www.youtube.com/embed/NL7iLFnt_Xg" class="active"><img src="images/video/video1.jpg"></a> </div>
                                                            </li>                   
                                                        </ul>
                                                    </div>
                                                    <a class="buttons next" href="#">right</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h2 class="title">album Din in the dark</h2>
                                    <div class="row-img">
                                        <a class="lb-image-link" href="images/lightbox/image-1.jpg" data-lightbox="lb-1"><img src="images/lightbox/thumb-1.jpg" alt="thumb-1" width="185" height="118"/></a>
                                        <a class="lb-image-link" href="images/lightbox/image-2.jpg" data-lightbox="lb-2"><img  src="images/lightbox/thumb-2.jpg" alt="thumb-2" width="185" height="118"/></a>
                                        <a class="lb-image-link" href="images/lightbox/image-3.jpg" data-lightbox="lb-3"><img src="images/lightbox/thumb-3.jpg" alt="thumb-3" width="185" height="118"/></a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li data-page-id="6">
                            <div id="breadcrumb">Press</div>
                            <div class="page-content">
                                <div class="scroll-pane">
                                    <h2 class="title">SAMPLE PEER REVIEW PRESS RELEASE</h2>
                                    <p>
                                        Dine in the Dark (DID) is a fine dining restaurant with a unique concept. Guests dine in a dining room in complete darkness. The dinner is a real sensory experience, where all non-visual senses are exacerbated. It is also an interesting, fun and enjoyable experience. The concept and venue is sophisticated, yet the feel is relaxed and casual.
                                    </p>
                                    <p>
                                        Below is a sample news release which may be used to announce the results of your system review in local newspapers. If this press release or the sample letter to clients is used, you should make the report available to those who request it.
                                    </p>
                                    <p>
                                        (FIRM LETTERHEAD)<br>
                                        FOR IMMEDIATE RELEASE<br>
                                        CONTACT: J. Doe<br>
                                        (123) 456 7890<br>
                                        (CPA FIRM NAME) HAS SUCCESSFUL REVIEW BY PEERS
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li data-page-id="7">
                            <div id="breadcrumb">Reservations</div>
                            <div class="page-content">
                                <div class="scroll-pane">
                                    <h2 class="title">Reservations</h2>
                                    <p>
                                        For special events or groups larger than 15 people, or if you do not see the date, time or group 
                                        size you are looking for, please call DID - Dine in the Dark at +66-(0)2-676-6676 during our 
                                        office hours from 11:00am to 11:00pm 7 days a week.
                                    </p>
                                    <p>RESERVATION FORM</p>
                                    
                                        <div class="reservation-content">
                                            <form action="#">
                                                <div class="row">
                                                    <div class="col col-6">
                                                        <label>Your name <span>*</span></label>
                                                        <input type="text" name="name" value="">
                                                    </div>
                                                    <div class="col col-6">
                                                        <label>Your e-mail address <span>*</span></label>
                                                        <input type="text" name="name" value="">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col col-3">
                                                        <label>Your Phone <span>*</span></label>
                                                        <input type="text" name="phone"value="">
                                                    </div>
                                                    <div class="col col-3 Zebra_DatePicker_Icon_Wrapper">
                                                        <label>Calendar <span>*</span></label>
                                                        <input id="datepicker" type="text" name="calendar" readonly="readonly" value="">
                                                    </div>
                                                    <div class="col col-3">
                                                        <label>Time <span>*</span></label>
                                                        <div class="select-style">
                                                            <select name="time" id="">
                                                            <option value="18.00">18.00</option>
                                                            <option value="18.15">18.15</option>
                                                            <option value="18.30">18.30</option>
                                                            <option value="18.45">18.45</option>
                                                            <option value="19.00">19.00</option>
                                                            <option value="19.15">19.15</option>
                                                            <option value="19.30">19.30</option>
                                                            <option value="19.45">19.45</option>
                                                            <option value="20.00">20.00</option>
                                                            <option value="20.15">20.15</option>
                                                            <option value="20.30">20.30</option>
                                                            <option value="20.45">20.45</option>
                                                            <option value="21.00">21.00</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col col-3">
                                                        <label>Group size <span>*</span></label>
                                                        <div class="select-style">
                                                            <select name="groupsize" id="">
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11"> > 10 (describe below)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col col-12">
                                                        <label>Other requirement</label>
                                                        <textarea name="requirement" id=""></textarea>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col col-12">
                                                        <label>Reservation policy <span>*</span></label>
                                                        <ul class="disc">
                                                            <li>
                                                                Clarifying any date-specific details that might need to be clear, for example, if the date they have chosen is for a specific event and if they might need to call in directly for more reservation details.
                                                            </li>
                                                            <li>
                                                                Options of personalizing their dining experience, for example, for birthdays, corporate events, or other functions.
                                                            </li>
                                                            <li>
                                                                Explaining the guest arrival and seating process.
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col col-6 align-right"><button class="btn btn-default">Send</button></div>
                                                    <div class="col col-6 align-left"><button type="reset" class="btn btn-default">Reset</button></div>
                                                </div>
                                            </form>
                                        </div>
                                </div>
                            </div>
                        </li>
                        <li data-page-id="8">
                           <div id="breadcrumb">Contact Us</div>
                                <div class="page-content">
                                    <div class="scroll-pane">
                                    <h2 class="title">Contact Us</h2>
                                    <div class="contact-content">   
                                        <div class="map-content">
                                          <div id="map_canvas"><iframe id="iframe1" width="627" height="290" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div>
                                        </div>
                                        <div class="row">
                                            <div class="col col-6">
                                                <p>CONTACT US FORM</p>
                                            </div>
                                            <div class="col col-6">
                                                <p>CONTACT US INFORMATION</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col col-6">
                                                <div class="row">
                                                    <div class="col col-12">
                                                        <label>Your name <span>*</span></label>
                                                        <input type="text" name="name" value="">
                                                   
                                                        <label>Your e-mail address <span>*</span></label>
                                                        <input type="text" name="name" value="">
                                                   
                                                        <label>Your phone </label>
                                                        <input type="text" name="name" value="">
                                                    
                                                        <label>Subject</label>
                                                        <textarea rows="8" cols="30"></textarea>
                                                    </div>
                                                </div>   
                                                <div class="row">
                                                    <div class="col col-12"><button class="btn btn-default">Send</button></div>
                                                </div>
                                            </div>
                                            <div class="col col-6">
                                                <div class="col col-12">
                                                  <p>
                                                     Dine in the Dark is a unique fine dining restaurant that heightens your senses. 
                                                        Discover a variety of Thai and international dishes without seeing them. Link 
                                                        your experience with a social purpose by supporting the visually impaired.
                                                    </p>
                                                    <p>
                                                     Ascott Sathorn Bangkok, 2nd Floor 187, South Sathorn Road,
                                                        Yannawa, (BTS Chong Nonsi)
                                                        Bangkok 10120
                                                    </p>
                                                    <p>
                                                     Telephone : +66 2676 6676 <br>
                                                        Restaurant Type : Supper/Night Dining, Fine Dining<br>
                                                        Opening Days : Daily<br>
                                                        Opening Hour : 06.30 P.M. - 11.00 P.M.
                                                    </p>
                                                </div>
                                             </div>
                                         </div>
                                    </div>                                    
                                </div>
                            </div>
                        </li>
                        <li data-page-id="9">
                            <div id="breadcrumb">FAQs</div>
                            <div class="page-content">
                                <div class="scroll-pane">
                                    <h2 class="title">FREQUENCY ARKED QUESTION</h2>
                                    <div class="faq-box">
                                        <strong>How dark is it inside the restaurant?</strong>
                                        <p>
                                            Din