import React from 'react';
import ReactDOM from 'react-dom';
import { store } from 'Redux/store';
import Stage from './components/Stage/';
import { Provider } from 'react-redux';
require('./style/main.scss');

ReactDOM.render(
  <Provider store={store}> 
    <Stage />
  </Provider>,
  document.getElementById('app'));

