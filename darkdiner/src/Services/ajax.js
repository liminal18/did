const PRODUCTION = process.env.NODE_ENV.toUpperCase() === 'PRODUCTION';
const production_url = "//" + window.location.host + "/api";
const baseUrl = PRODUCTION ? production_url : 'http://127.0.0.1:8000';

/**
* dataToJson turns the form data into a json string
* @ param {string} selector -> the selector for the form
*/
function dataToJson(selector){ 
  const result = jQuery(selector).serializeArray();
  const dic = result
    .reduce((acc, c) => {
          const n = {};
          n[c.name] = c.value;
          return Object.assign(acc, n);
        }, {})
      return JSON.stringify(dic);
};

/**
* ajax post
* @ param {string} the route we are posting to
* @ param {string} the selector for the form
* @ param {Function} a callback function
*/
module.exports = function(route, selector, submitCallback){
  jQuery.ajax({
    url: baseUrl + route,
    dataType: 'json',
    contentType: 'application/json; charset=UTF-8',
    data: dataToJson(selector),
    type: 'POST',
    cache: false,
    success(response) {
      if (submitCallback) {
        submitCallback(response, true);
      }
    },
    error(response) {
      if (submitCallback) {
        submitCallback(response, false);
      }
    },
  });
}
