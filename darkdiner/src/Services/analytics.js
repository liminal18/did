/**
* sends an event to google analytics
* @ param {String} the event category
* @ param {String} the action name
* @ param {String} label
* @ param {String} value
* @ param {String} fields
*/

exports.sendGaEvent = function (category, action, label, value, fields) {
  if (typeof window.ga === 'function') {
    window.ga('send', 'event', category, label, value, fields);
  }
};

/**
* send FB Event to facebook pixel
* @ param {String} category
* @ param {String} the field names
* @ param {Boolean} should we use a custom event?
*/

exports.sendFBevent = function (category, fields, custom) {
  if (typeof window.fbq === 'function') {
    if (custom) {
      window.fbq('trackCustom', category, fields);
      return true;
    }
    if (!fields) {
      window.fbq('track', category);
      return true;
    }
    if (fields) {
      window.fbq('track', category, fields);
      return true;
    }
  }
  return false;
};
