/**
* sorts an array of objects by property returning it min to max
* intended to be used by the .sort method on an array
* @params {String} property
* @params {Object, Object} a, b
*/

const minSort = property => (a, b) => a[property] - b[property];
exports.minSort = minSort;

/**
* sorts an array of objects by property returning it max to min
* @params {String} property
* @params {Object, Object} a, b
*/

exports.maxSort = property => (a, b) => minSort(property)(b, a);

/**
* sorts the array using Date first and likes second
* max Date first then max likes
* @ param {Object}
* @ param {Object}
* Where Object = { day: Date, likes: int, source: img url }
*/

exports.DateThenLikes = (a, b) => {
  if (a.date !== b.date) {
    return b.date - a.date;
  }
  return b.likes - a.likes;
};
