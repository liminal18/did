const MobileDetect = require('mobile-detect');

const ua = navigator.userAgent;
const md = new MobileDetect(ua);
window.md = md;
exports.isMobile = md.mobile();
exports.ua = ua;
exports.isAndroid = ua.indexOf('android') > -1;
exports.getHalf = function (v1, v2) {
  return Math.floor(v1 / v2);
};
