import React from 'react';
const { store } = require('Redux/store');
const { validEmail, validPhone } = require('../../../../Services/validators');
const stripToNums = /[^\d]/g;
import Reservation_Success from '../Success/index.jsx';
import TimePicker from '../TimePicker/index.jsx';
import GroupSelect from '../GroupSelect/index.jsx';
import PickADate from '../PickADate/index.jsx';
import PostToApi from 'Services/ajax.js';
import Error from 'Components/Error/index.jsx';
const { sendGaEvent, sendFBevent } = require('Services/analytics');

class Booking {
  constructor(group, name, phone, email, error) {
    this.group = group;
    this.name = name;
    this.phone = phone;
    this.email = email;
    this.error = error;
    this.time = '18:30';
    this.calendar = new Date();
  }
}

class ReservationForm extends React.Component{
  constructor(props){
    super(props);
    this.state = new Booking(1, null, null, null, {submit: this.props.error});
 }  
 today() {
    const today = new Date();
    const month = today.getMonth() < 10 ? `0${today.getMonth()}` : today.getMonth();
    const day = today.getDate() < 10 ? `0${today.getDate()}` : today.getDate();
    return `${today.getFullYear()}-${month}-${day}`;
  }
  getMonth() {
    const today = new Date();
    const month = today.getMonth() < 10 ? `0${today.getMonth()}` : today.getMonth();
    return `${today.getFullYear()}-${month}-01`;
  }
  formReset() {
    document.getElementById('ReservationForm').reset();
    this.state.error = {};
    this.state.phone = null;
    this.state.name = null;
    this.state.email = null;
    this.setState(this.state);
  }
  validate() {
    const phoneRegEx = new RegExp(validPhone);
    this.state.error = {};
    if (!this.state.name) {
      this.state.error.name = true;
    }
    if (!this.state.phone) {
      this.state.error.phone = "Phone Number Required";
    }
    // strip everything but numbers then check the phone number
    if(this.state.phone && !phoneRegEx.test(this.state.phone.replace(stripToNums,''))){
      this.state.error.phone = "Phone Numbers Must Contain 3 to 15 digits";
    }
    if (!this.state.email) {
      this.state.error.email = true;
    }
    const keys = Object.keys(this.state.error);
    if (keys.length < 1) {
      return true;
    }
    this.setState(this.state);
    return false;
  }
  submitCallback(result, success) {
    this.state.error.submit = !success;
    if (success) {
      sendFBevent('CompleteRegistration');
      return this.props.success(true);
    } 
    return this.props.success(false, result);
  }
   submit(loc, callBack) {
    if (!this.validate()) { return false; }
    this.props.loading();
    PostToApi(`/reservation?loc=${loc}`, '#ReservationForm', callBack);
  }
  addToState(k) {
    const that = this;
    return function (el) {
      const value = el.target.value;
      that.state[k] = value;
      that.state.error[k] = false;
      that.setState(that.state);
    };
  }
  changeDate(date) {

  }
  render() {
    if (this.state.success == true) {
      return(<Reservation_Success back={this.changeSuccess} />);    
    }
    return (
      <form 
        id="ReservationForm" 
        className="form-validation center-block col-xs-12"
        onSubmit={(e) => {
              e.preventDefault();
              this.submit(this.props.location, this.submitCallback.bind(this));
        }}
        autoComplete="on">
        { this.state.error.submit ? <Error errors={this.state.error.submit} /> : null}
        <div className="form-group col-xs-12">
          <label htmlFor="FullName" className="col-xs-6"> Your name:
            <input id="FullName" name="FullName" type="text" 
              className="form-control" onChange={this.addToState('name')} autoComplete='name' />
            { this.state.error.name ? <div className='text-danger'>Name Required</div> : null }
          </label>
          <label htmlFor="EmailReservation" className="col-xs-6"> Your email:
            <input id="EmailReservation" name="EmailReservation" pattern={validEmail} 
              type="email" className="form-control" onChange={this.addToState('email')} autoComplete='email' />
            { this.state.error.email ? <div className='text-danger'>Email Required</div> : null }
          </label>
        </div>
        <div className="form-group col-xs-12">
          <label htmlFor="PhoneReservation" className="col-xs-6" > Your phone:
            <input id="PhoneReservation" name="PhoneReservation" minLength={3} maxLength={20} 
              className="form-control" type="tel" 
              onChange={this.addToState('phone')} autoComplete='tel' />
            { this.state.error.phone ? 
                <div className='text-danger'> {this.state.error.phone} </div> : null }
          </label>
          <label htmlFor="Calendar" className="col-xs-6" > Date:
            <PickADate changeDate={this.changeDate} location={this.props.location} />
          </label>
        </div>
        <div className="form-group col-xs-12">
          <label htmlFor="Time" className="col-xs-6"> Time:
            <TimePicker change={this.addToState('time')} time={this.state.time} />
          </label>
          <label htmlFor="Group" className="col-xs-6"> Group size:
            <GroupSelect change={this.addToState('group')} size={this.state.group} />
          </label>
        </div>
        <div className="form-group col-xs-12">
          <label htmlFor='Notes' className='col-xs-12'>Dietary or other requirements
            <textarea id='Notes' name='Notes' className='col-xs-12 text-black' />
          </label>
        </div>
        <div className="btn-group col-xs-8 col-xs-offset-2">
          <button className="btn btn-default col-xs-6" type="submit">
           Submit
          </button>
          <button className="btn btn-default col-xs-6" type="button" onClick={() => this.formReset()}>
          Reset
          </button>
        </div>
      </form>
    );
  }
};

export default ReservationForm;
