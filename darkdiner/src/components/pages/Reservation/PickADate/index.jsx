import React from 'react';
const moment = require('moment');
const DatePicker = require('react-datepicker');

class PickADate extends React.Component {
  constructor(props){
    super(props);
    this.state = { start: moment() };
  }
  changeDate(date) {
    this.setState({ start: date });
    this.props.changeDate(date.format('DD-MM-YYYY'));
  }
  disableDays(loc) {
    if (loc == 1) {
      const disableDays = [];
      for (let i = 7; i < 180; i += 7) {
        //Bkk is not open on Sundays
        disableDays.push(moment().day(i));
      }
      return disableDays;
    }

    return null;
  }
  render() {
    return (
      <div className="col-xs-12 col-md-12 no-padding">
        <DatePicker dateFormat="DD/MM/YYYY" selected={this.state.start} 
          onChange={this.changeDate.bind(this)} minDate={moment()} 
          excludeDates={this.disableDays(this.props.location)} className="col-xs-12 form-control" />
        <input
          id="Calendar" name="Calendar" type="text" className="form-control"
          value={this.state.start.format('DD-MM-YYYY')} readOnly="true" style={{ display: 'none' }}
        />
      </div>
    );
  }
};

export default PickADate;
