import React from 'react';
import { store } from 'Redux/store';

const Reservation_Success = (props) => {
    return (
      <div id="Reservation_Success" className="animated fadeIn">
        <i className="fa fa-check-circle fa-5x" aria-hidden="true"></i>
        <h2>Reservation Request Sent</h2>
        <p>Thank you for your interest in DID – Dine in the Dark.</p>
        <p>Our team will check the availability of seats and will email you in the near 
        future with a booking confirmation or alternative dates or times.
        </p>
        <button className='btn btn-primary col-xs-6 col-xs-offset-3' 
          onClick={() => store.dispatch({ type: 'CHANGE_SECTION', section: 'HOME' })}>
         OK
        </button>
      </div>
    );
};

export default Reservation_Success;
