import React from 'react';

const TimePicker = (props) => {
  const getEl = (e) => {
    props.change('time', e);
  };
  const times = ['18:00', '18:30', '19:00', 
    '19:30', '20:00', '20:30', '21:00', '21:30'];
  const makeOptions = (time, i) => {
    return(<option key={time + i} value={time}>{time}</option>);
  };
    return (
      <span>
        <select className='form-control' onChange={props.change}>
          {times.map(makeOptions)}
        </select>
        <input 
          id='Time' 
          name='Time' 
          type='time' 
          className='form-control' 
          min='18:30' 
          max='23:00' 
          step='600'
          value={props.time ? props.time : '18:30'}
          autoComplete='off'
          readOnly={true}
          style={{display: 'none'}}
        />
      </span>
   );
};
export default TimePicker;
