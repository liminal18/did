const React = require('react');
import ReservationForm from './Form/index.jsx';
const { sendGaEvent, sendFBevent } = require('../../../Services/analytics');
import Success from './Success/index.jsx';
import Loading from 'Components/Loader/';

/***
* Reservation_Page
*/

module.exports = class ReservationPage extends React.Component {
  constructor(props){
    super(props);
    this.state = {success: false, loading: false, error: false};
  }
  changeSuccess(success, errors){
    const getError = (errors) => errors.responseJSON ? errors.responseJSON : true;
    this.setState({
      success: success, 
      loading: false, 
      error: errors ? getError(errors) : false
    });
  }
  changeLoading(){
    this.setState({loading: !this.state.loading});
  }
  componentDidMount(){
    // implement facebook if needed
    window.changeLoading = this.changeLoading;
  }  
  render() {
    if(this.state.loading){
      return(
        <div>
          <Loading />
        </div>
      );
    }
    if(this.state.success){
      return(
        <div className='col-xs-12'>
          <div className='col-xs-12 spacer-p-2 spacer-l-2' />
          <Success back={this.changeSuccess} />
          <div className='col-xs-12 spacer-p-2 spacer-l-2' />
        </div>
      );
    }
    switch (this.props.location) {
      case 1:
        return (
          <div id="reservation-page" className="text-purple text-left animated slideInUp">
            <span className="col-xs-12 no-padding">
              <div className="col-xs-4" />
              <h2 className="did-title col-xs-12 no-padding">Reservations</h2>
            </span>
            <p>
            We kindly encourage our guests to book their seats in advance.
            Please complete the online form below and we will confirm your requested 
            reservation via email as soon as possible.
            </p>
            <p> 
            The Sheraton Grande Sukhumvit is able to cater for many types of dietary requirements;
            please let us know of any mild allergies or specific requirements. If your requested 
            reservation is for the same day, please contact the Sheraton Grande Sukhumvit directly
            via telephone at <a href="tel:+66026498358"> +66 (0) 2649 8358.</a> For special events 
            or groups of more than 20 persons, please call us or email us at 
            <a href="mailto:bangkok@didexperience.com"> bangkok@didexperience.com. </a>
            </p>
            <p>
            After your reservation has been confirmed by us, you can pre-purchase dinners and 
            benefit from discounts of up to 20% by visiting the Sheraton Grande Sukhumvit’s 
            <a href="//www.sheratongrandesukhumvit.com/en/store/dineinthedark"> online store</a>.
            </p>
            <div className="col-xs-12 spacer-p-1 spacer-l-2" />
            <h4>RESERVATION FORM</h4>
            <ReservationForm 
              location={this.props.location} 
              success={this.changeSuccess.bind(this)} 
              loading={this.changeLoading.bind(this)}
              error={this.state.error}
            />
            <div className="col-xs-12 spacer-p-2 spacer-l-4" />
            <div className="col-xs-12" />
          </div>
        );
      case 2:
        return (
          <div id="reservation-page" className="text-purple text-left animated slideInUp">
            <span className="col-xs-12 no-padding">
              <div className="col-xs-4" />
              <h2 className="did-title col-xs-12 no-padding">Reservations</h2>
            </span>
            <p>
            We kindly encourage our guests to book their seats in advance. 
            Please complete the online form below and we will confirm your 
            requested reservation via email as soon as possible.            
            </p>
            <p>
            We are able to cater for many types of dietary requirements; please let us 
            know of any mild allergies and specific requirements. If your requested 
            reservation is for the same day, please contact us directly via telephone 
            at <a href="tel:+855077589458.">+855 (0)77 589 458.</a> 
            For special events or groups of more than 12 persons, please call us or
            email us at <a href='mailto: phnompenh@didexperience.com'> phnompenh@didexperience.com </a>.            
            </p>
            <div className="col-xs-12 spacer-p-2 spacer-l-4" />
            <h4>RESERVATION FORM</h4>
            <ReservationForm 
              location={this.props.location} 
              success={this.changeSuccess.bind(this)} 
              loading={this.changeLoading.bind(this)}
              error={this.state.error}
            />
            <div className="col-xs-12 spacer-p-1 spacer-l-2" />
            <div className="col-xs-12" />
          </div>
        );
      default:
        return (
          <div id="reservation-page" className="text-purple text-left animated slideInUp">
            <span className="col-xs-12 no-padding">
              <div className="col-xs-4" />
              <h2 className="did-title col-xs-12 no-padding">Reservations</h2>
            </span>
            <p>
            We kindly encourage our guests to book their seats in advance.
            Please complete the online form below and we will confirm your requested 
            reservation via email as soon as possible. 
            </p>
            <p> 
            The Sheraton Grande Sukhumvit is able to cater for many types of dietary requirements;
            please let us know of any mild allergies or specific requirements. If your requested 
            reservation is for the same day, please contact the Sheraton Grande Sukhumvit directly
            via telephone at <a href="tel:+66026498358"> +66 (0) 2649 8358.</a> For special events 
            or groups of more than 20 persons, please call us or email us at 
            <a href="mailto: bangkok@didexperience.com">bangkok@didexperience.com. </a>
            </p>
            <h4>RESERVATION FORM</h4>
            <ReservationForm 
              location={this.props.location} 
              success={this.changeSuccess.bind(this)}
              loading={this.changeLoading.bind(this)}
              error={this.state.error}
            />
            <div className="col-xs-12 spacer-p-1 spacer-l-2" />
            <h4>RESERVATION POLICY</h4>
            <p>
            DID – Dine in the Dark is a reservation driven restaurant. 
            We welcome walk-in guests and will accommodate them as much as possible.
            </p>
            <p>
            Parties that are late for their reservation time are given thirty minutes before 
            we attempt to contact the person that made the reservation.
            Given the limited seating of the restaurant, forty-five minutes late with no 
            contact will void your reservation. If you are late, we urge you to contact us so 
            we can make our best effort to accommodate you and your party.
            Kindly inform us in advance by e-mail or phone in the case of changes 
            to your reservation or cancellations.
            </p>
            <p> 
            Please call DID – Dine in the Dark at <a href="tel:+85577589458"> +855 775 894 58</a> 
            during our office opening hours between 08:00 – 22:00 (08:00am – 10:00pm)
            every day of the week; for the following inquiries.
            </p>
            <ul>
              <li>
              For special events or groups larger than 10 people.
              For large group bookings a deposit may be required when the reservation is made, 
              please discuss this with our staff when making your reservation.
            </li>
              <li>
              If you do not see the specific date, time or group size you are looking for 
              </li>
              <li>
              If you have not received a confirmation email regarding your reservation within 48 hours of your booking 
              </li>
              <li> 
              Special events, corporate events, birthdays, or other private functions
              </li>
            </ul>
          </div>
        );
    }
  }
};
