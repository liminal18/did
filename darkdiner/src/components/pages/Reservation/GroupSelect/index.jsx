import React from 'react';

const GroupSelect = (props) => {
  const sizes = [...Array(20).keys()];
  const makeOptions = (size, i) => {
    return (<option key={size} value={size + 1}>{size + 1}</option>);
  };
    return (
      <span>
        <select className="form-control" onChange={props.change}>
          {sizes.map(makeOptions)}
        </select>
        <input 
          id="Group" 
          name="Group" 
          type="number" 
          min="1" 
          max="20" 
          value={props.size} 
          style={{ display: 'none' }} 
          readOnly 
        />
        <div 
          style={{ display: props.size >= 20 ? 'inline' : 'none' }} 
          className="text-danger">
          For more than 20 Email Us
        </div>
      </span>
    );
};

export default GroupSelect;
