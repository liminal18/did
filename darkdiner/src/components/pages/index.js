import React from 'react';
import { ImageWaiter } from '../mixins/';
import Modal_Image from '../Modal_Image/';
import { sendGaEvent, sendFBevent } from 'Services/analytics';
import YouTubeEmbed from '../embed/youtube';
import { changeSection } from 'Redux/store';
import { FacebookAlbum } from '../embed/facebook';

exports.Reservation_Page = require('./Reservation/index.jsx');
exports.ContactOrMap = require('./contactmap');

exports.FAQ_Page = class FAQPage extends React.PureComponent {
  constructor(props){
    super(props);
    this.FaqElements = [
    { loc: 1,
      title: 'What is your restaurants capacity?',
      text: 'We can seat 50 people simultaneously and up to 150 people throughout the evening with multiple seating times and entertainment in the welcome lounge. Our space can be divided into separate private dining areas.' },
    { loc: 2,
      title: 'What is your restaurants capacity?',
      text: 'We can seat 25 people simultaneously and up to 50 people throughout the evening with multiple seating times and entertainment in the welcome lounge, bar and lush gardens on the ground floors.' },
    { loc: 1,
      title: 'Are you open for lunch?',
      text: 'We are not open for lunch; our regular hours are from 18:30 hours until 23:00 hours, with the last seating time at 21:30 hours. For large groups, we can organise special daytime events such as brunch, lunch or afternoon activities. Please contact us for more information.' },
    { loc: 2,
      title: 'Are you open for lunch?',
      text: 'We are not open for lunch; our regular hours are from 18:00 hours until 23:00 hours, with the last seating time at 21:30 hours. However, for groups of 12 persons and above, we can organise special daytime events such as brunch, lunch or afternoon activities. Please contact us for more information.' },
    { loc: 0,
      title: 'Can I take my phone inside the restaurant?',
      text: 'Our policy is that guests may not take any objects that emit light into the restaurant. This includes items such as switched-on mobile phones, cameras, lighters, and light-emitting watches. We require guests to switch off their mobile phones and we also have safe deposit areas for other such items in our welcome lounge. Should you expect an important phone call, please let our staff know and they will inform you in case you would receive such call during your dinner.' },
    { loc: 1,
      title: 'What type of food do you serve?',
      text: 'We offer a choice between four delicious 4-course surprise menus, including Asian, Western, Vegetarian, and the special Chef’s Selection.' },
    { loc: 2,
      title: 'What type of food do you serve?',
      text: 'We offer a choice between four delicious 4-course surprise menus, including Khmer, International, Vegetarian, and the Chef’s Monthly Selection.' },

    { loc: 1,
      title: 'How much is a dinner at DID – Dine in the Dark?',
      text: 'All our 4-course surprise menus are priced 1,450++ Thai Baht. We also offer a wide selection of à-la-carte wines, surprise cocktails, beers, liquors and soft drinks.' },
    { loc: 2,
      title: 'How much is a dinner at DID – Dine in the Dark?',
      text: 'All our 3-course surprise menus are priced US$ 18.00+. We also offer a wide selection of à-la-carte wines, surprise cocktails, beers, liquors and soft drinks.' },
    { loc: 0,
      title: 'What happens if I have an allergy or dislike something?',
      text: 'Any mild allergies and dietary restrictions should be mentioned when making the reservation and be reported to the host/hostess on arrival.' },
    { loc: 0,
      title: 'How do I eat and drink my food and beverage in the dark?',
      text: 'Our visually impaired guides will lead you into the restaurant and to your seat and table. They will then explain where your cutlery, dishes and drinks are located on the table. Once used to your surroundings, eating in the dark becomes an easy and enjoyable experience.' },
    { loc: 0,
      title: 'Do you serve alcohol?',
      text: 'Yes! Dine in the Dark has an extensive a-la-carte wine list; cocktails and soft drinks are also available. These can be ordered in the lit area before dinner, or during your meal.' },
    { loc: 0,
      title: 'What happens if I spill something?',
      text: 'Guests generally do not spill anything as people tend to be much more careful in the dark. In the rare case of something spilling, our guides will help with the cleaning process. Although these are usually not necessary for enjoying a problem-free dining experience itself, we also provide signature DID-branded chef aprons upon request.' },
    { loc: 1,
      title: 'Do you have a smoking area?',
      text: 'DID – Dine in the Dark is a non-smoking venue. Guests may smoke in the outdoor areas at the entrance to the building.' },
    { loc: 2,
      title: 'Do you have a smoking area?',
      text: 'DID – Dine in the Dark is a non-smoking venue. Guests may smoke in the pleasant outdoor garden areas in the front and back areas of our restaurant.' },
    { loc: 1,
      title: 'Are there restrooms at the restaurant and how do we excuse ourselves in the dark?',
      text: 'There are restrooms located at BarSu, the welcome lounge and bar area. Should you wish to exit the restaurant at any time, simply call one of our guides by their name and they will lead you back into the lit area.' },
    { loc: 2,
      title: 'Are there restrooms at the restaurant and how do we excuse ourselves in the dark?',
      text: 'There are restrooms located next to our welcome lounge area. Should you wish to exit the restaurant at any time, simply call one of our guides by their name and they will lead you back into the lit area.' },
    { loc: 1,
      title: 'Are children allowed to Dine in the Dark?',
      text: 'The general rule is that children of the age of 12 or above are able to experience DID – Dine in the Dark, if accompanied by adults.' },
    { loc: 2,
      title: 'Are children allowed to Dine in the Dark?',
      text: 'The general rule is that children of the age of 10 or above are able to experience DID – Dine in the Dark, if accompanied by adults. Please contact us for more information or special circumstances.' },
    { loc: 1,
      title: 'Do we have to tip?',
      text: 'There is a 10% service charge and 7% government tax included on the final bill. Guests are welcome to tip beyond that, at their own discretion' },
    { loc: 2,
      title: 'Do we have to tip?',
      text: 'There is a 10% service charge included in the final bill. Guests are welcome to tip beyond that, at their own discretion.' },
    { loc: 1,
      title: 'Do you have live music?',
      text: 'We have a number of musicians available for your entertainment at an extra cost for private parties. We also regularly host special live music event; please call us to find out what’s happening tonight!' },
    { loc: 2,
      title: 'Do you have live music?',
      text: 'There are a number of musicians available for your entertainment at an extra cost for private parties. Please contact Dine in the Dark to find out what’s happening tonight!' },
    { loc: 1,
      title: 'Can you accommodate a specific themed dinner?',
      text: 'Yes! We are very creative when it comes to develop tailor-made dining experiences and you can also make use of the adjacent lit BarSu lounge and bar area for larger group outings and functions. The space can be transformed with visuals, live music, special activities and entertainment, to fit your concept ideas.' },
    { loc: 2,
      title: 'Can you accommodate a specific themed dinner?',
      text: 'Yes! We are very creative when it comes to develop tailor-made dining experiences and you can also make use of our welcome lounge and bar area, as well as our lush gardens, for larger group outings and functions. The space can be transformed with visuals, live music, special activities and entertainment, to fit your concept ideas.' },
    { loc: 1,
      title: 'Do you have parking spaces available?',
      text: 'The Sheraton Grande Sukhumvit has parking available for cars and space available for larger vehicles such as coaches. Guests are able to park their cars provided they get their ticket stamped on departure.' },
    { loc: 2,
      title: 'Do you have parking spaces available?',
      text: 'Parking is also available in a side street nearby if you come with your own car or for larger vehicles such as coaches; speak with a member of our team upon arrival.' },
    { loc: 1,
      title: 'What are the closest public transportation stations to DID - Dine in the Dark',
      text: 'The closest BTS station is Asoke (walk towards, and pass by, BTS Exit 2) and the closest MRT station is Sukhumvit (MRT Exit 3). There is a direct sky-walk pedestrian access from those stations to the lobby area of the Sheraton Grande Sukhumvit.' },
    { loc: 0,
      title: 'What happens in the case of an emergency?',
      text: 'The DID staff are all professionally trained to react in the rare situation of an emergency. The lights will be turned on in the dining area and the DID staff will calmly direct guests towards the available emergency exits' },
    { loc: 1,
      title: 'Do you have commission rates for tour operators?',
      text: 'Yes! We have different rates depending on the type of package sold. Please our rates for tour operators' },
    { loc: 2,
      title: 'Do you have commission rates for tourism professionals?',
      text: <span>Yes! There are different rates depending on the type of package sold. Please contact Dine in the Dark for their rates for tour operators and travel agents via <a href='mailto:info@DIDexperience.com'>info@DIDexperience.com</a></span>},
    { loc: 0,
      title: 'Do you have a franchise system or enter into partnerships to open new DID – Dine in the Dark restaurants?',
      text: <span>Yes, we are open to explore new opportunities and develop new branches in Thailand or abroad. Please explore this website’s <a href='javascript:void(0)' className='c-hand' onClick={() => changeSection('Franchise')} >Franchise section</a> accessible at the lower end of your screen and contact us at <a href="mailto:info@DIDexperience.com">info@DIDexperience.com</a> to learn more about the benefits we offer and discuss your projects with us.</span> },
  ]

  }
  componentDidMount() {
    sendFBevent('ViewContent', { content_name: 'faq' });
  }
    parseLocation(value, index, array) {
    if (value.loc != this.props.location && value.loc != 0) { return null; }
    return(
      <div key={'parsedLocation_' + index}>
        <div className='text-bold'>{value.title}</div>
        <div className='faq-text'>{value.text}</div>
        <div className='spacer-l-3' />
        <br />
      </div>
    );
  }
  render() {
    return (
      <div id='FAQ-Page' className='text-left text-purple animated slideInUp'>
        <span className='col-xs-12 no-padding'>
          <div className='col-xs-12'>
            <div className='col-xs-4' />
          </div>
          <h2 className='did-title col-xs-12 no-padding'> FAQS - Frequently Asked Questions</h2>
        </span>
        { this.FaqElements.map(this.parseLocation.bind(this))}
      </div>
   );
  }
};

exports.Career_Page = class CareerPage extends React.PureComponent {
  constructor(props){
    super(props);
  }
  componentDidMount() {
    sendFBevent('ViewContent', { content_name: 'career' });
  }
  getLocEmail(loc) {
    switch (loc) {
      case 1:
        return 'info@DIDexperience.com';
      case 2:
        return 'PhnomPenh@DIDexperience.com';
      default:
        return 'info@DIDexperience.com';
    }
  }
  getCountry(loc) {
    switch (loc) {
      case 1:
        return 'Thailand';
      case 2:
        return 'Cambodia';
      default:
        return 'Thailand';
    }
  }
  render() {
    const location = this.props.location;
    return (
      <div 
        id='Carrer-Page' 
        className='text-left text-purple animated slideInUp'
      >
        <h2 className='did-title no-padding col-xs-12'>
          Careers at DID - Dine in the Dark
        </h2>
        <p>
DID – Dine in the Dark has a social purpose and supports the principles of
fair and equal employment and opportunity. We aim to provide a culture of 
growth, motivation, and empowerment. We are ideally looking for people 
with hospitality backgrounds, but above all, people with great attitudes, 
an optimistic view on life and a strong desire to be part of an evolving 
brand that makes a social impact in {this.getCountry(location)}
        </p>
        <p>
If you would like an opportunity to join this incredible team please 
contact us via email with your CV or Resume attached, to <a 
  href={'mailto:' + this.getLocEmail(location) + '?subject=JOB APPLICATION'}>
  {this.getLocEmail(location)}
</a> with the subject 'JOB APPLICATION'</p>
      </div> 
    );
  }
};

exports.Franchise_Page = class FranchisePage extends React.PureComponent {
  constructor(props){
    super(props);
  }
  componentDidMount() {
    sendFBevent('ViewContent', { content_name: 'franchise' });
  }
  render() {
    return (
      <div id="Franchise_Page" className="col-xs-12 text-left text-purple">
        <span className="col-xs-12 no-padding">
          <h2 className="did-title col-xs-12 no-padding">Franchise</h2>
        </span>
        <YouTubeEmbed
          height="300"
          width="300"
          YouTubeLink="https://www.youtube.com/embed/lW_aefnqq-0"
        />
        <div className="col-xs-12 spacer-l-2 spacer-p-1" />
        <p>THE OPPORTUNITY</p>
        <p>
          We offer you the opportunity to implement our extraordinary concept restaurant at your venue, hotel or restaurant.
        </p>
        <p>
        The DID – Dine in the Dark brand is recognised internationally and adds immediate 
        financial and PR value to venues located in large cities or popular tourists destinations on any continent.        
        </p>
        <p>
        Since 2012, we have been the pioneers and leaders of dark dining restaurants in Asia, with 
        branches located at the Sheraton Grande Sukhumvit – A Luxury Collection Hotel – in Bangkok, 
        Thailand, and in Phnom Penh, Cambodia.
        </p>
        <p>
        The quality of our restaurants and the outstanding execution of a unique concept enables us 
        to attract a high number of guests every day and attract strong media attention. Our restaurants 
        also regularly rank in the top 10 of their cities’ TripAdvisor rankings and have become a 
        successful example of for-profit businesses that not only have a remarkable financial track 
        record, but also generate a significant and tangible social impact.
        </p>
        <div className='col-xs-12 spacer-l-4 spacer-p-2' />
        <p>A COST-EFFICIENT APPROACH</p>
        <p>
        Our unique approach is to use spaces that are little used in hotels and restaurants and to 
        convert these into dark dining restaurants in the evenings. Examples of such spaces include 
        function rooms, meeting rooms or breakfast areas.
        </p>
        <p>
        Operating a restaurant in the dark is a complex process and our experienced team is well-versed 
        in all dimensions of dark concept restaurants, including operations, training, menu development, 
        marketing, and the recruitment of visually impaired persons. We have the proven capacity to 
        develop and open a restaurant in partnership with existing venues in various cultural contexts 
        within a short period of time and at reasonable costs.
        </p>
        <div className='col-xs-12 spacer-l-4 spacer-p-2' />
        <p>A PARTNERSHIP</p>
        <p>
        You receive our support both during the development phase and the ongoing implementation of 
        the restaurant. We provide you with the business developers, trainers and managers to set up, 
        run and make the restaurant successful. You also receive ongoing assistance to develop surprise 
        menus, add new components enhancing the customer experience, recruit new visually impaired persons, 
        support the ongoing training of your team, as well as to develop new events, offers and marketing strategies
       </p>
       <p>
       We value strong and lasting partnerships and typically work on the basis of a
       percentage of the gross revenue. If you are interested in discussing the development of a new 
       branch with us or feel that your venue might be a good fit for hosting such restaurant,
       please contact us at <a href="mailto:info@didexperience.com">info@didexperience.com</a> and 
       we will be delighted to explore the opportunity with you. 
       </p>
      </div>
    );
  }
};

exports.Home_Page = class HomePage extends React.PureComponent {
  constructor(props){
    super(props);
    this.state = { text: null };
  }
  shouldComponentUpdate(next_props, next_state) {
    if (this.props.location != next_props.location) {
      return true;
    }
    return false;
  }
  getClasses(l) {
    const timediff = Date.now() - this.state.start;
    if (timediff > 2000) {
      return 'text-purple text-left';
    }
    if (l >= 3 && timediff < 45) {
      return 'text-purple text-left animated slideInUp';
    }
    return 'text-purple text-left animated fadeIn';
  }
  componentDidMount() {
    sendFBevent('ViewContent', { content_name: 'Home' });
  }
  render() {
    switch (this.props.location) {
      case 1:
        return (
          <div id="home-page" className="text-purple text-left animated fadeIn">
              <span className="col-xs-12 no-padding">
              <h2 className="did-title col-xs-12 no-padding">DID - Dine in the Dark</h2>
            </span>

            <p>Welcome to DID - Dine in the Dark at the Sheraton Grande Sukhumvit,
           a Luxury Collection Hotel, Bangkok. Prepare yourself to embark on a journey of
           sensory awareness served by the visually impaired. </p>
            <p>
            DID - Dine in the Dark is a fine dining restaurant in which there are no lights inside. 
            We offer you the chance to experience a variety of local and international surprise menus 
            without seeing them, thereby heightening your non-visual senses in a playful and elegant way.</p>
            <p>
            All members of our service team are visually impaired and fluent in Thai and English. 
            Our restaurant offers you a unique opportunity to support their employment and discover their professional skills.
            </p>
            <p>
            We are open Mondays to Saturdays, from 18.30 hours (last seating at 21.30 hours). 
            For reservations, please call <a href="tel:+66026498358"> +66 (0)2 649 8358</a> or 
            complete the online form provided in the <a href='javascript:void(0)' className="c-hand" onClick={() => changeSection('Reservations')}> 
            Reservations </a>section of this website.
            </p>
            <div className="row col-xs-12 spacer-p-1 spacer-l-2" />
            <div className="col-xs-12 no-padding flex-container">
              <Modal_Image url="Bangkok/Home/one.jpg" size="flex-image" alt="Follow the Guide" />
              <Modal_Image url="Bangkok/Home/two.jpg" size="flex-image" alt="Our Guides"/>
              <Modal_Image url="Bangkok/Home/three.jpg" size="flex-image"alt="Dining at DiD" />
            </div>
            <div className="row col-xs-12 spacer-p-1 spacer-l-2" />
          </div>
        );
      case 2:
        return (
          <div id="home-page" className="text-purple text-left animated fadeIn">
            <div className="col-xs-12 no-padding">
              <div className="col-xs-4" />    
              <h2 className="did-title col-xs-12 no-padding">
                DID - Dine in the Dark</h2>
            </div>
            <p>
            Welcome to DID - Dine in the Dark in Phnom Penh. Prepare yourself to embark on an 
            extraordinary journey of sensory awareness served by the visually impaired.            
            </p>
            <p>
            DID - Dine in the Dark is a fine dining restaurant in which there are no lights inside. 
            We offer you the chance to experience Khmer, international or vegetarian surprise menus 
            without seeing them, thereby heightening your non-visual senses in a playful and elegant way.
            </p>
            <p>
            All members of our service team are visually impaired and fluent in Khmer and English. 
            Our restaurant offers you a unique opportunity to support their employment and discover 
            their professional skills.
            </p>
            <p>
            We are open every day from 18.00 hours (last seating at 21.30 hours). 
            For reservations, please call <a className="c-hand" href="tel:+855 (0)77 859 458">
            +855 (0)77 859 458</a>, email <a className="c-hand" href="mailto:phnompenh@didexperience.com">
             phnompenh@didexperience.com</a> or complete the online form provided in the 
            <a href='javascript:void(0)' className="c-hand" onClick={() => changeSection('Reservations')}> Reservations </a> 
            section of this website. 
            </p>
              <div className="row col-xs-12 spacer-p-1 spacer-l-2" />
              <div className="col-xs-12 no-padding flex-container">
                <Modal_Image 
                  url="PhonmPenh/Home/1-Home-DID-DineintheDark-PhnomPenh.jpg" 
                  size="flex-image"
                  alt='A Guide' 
                />
                <Modal_Image 
                  url='PhonmPenh/Home/2-Home-DID-DineintheDark-PhnomPenh.jpg' 
                  size='flex-image'
                  alt='Your Guides For The Evening' 
                />
                <Modal_Image 
                  url="PhonmPenh/Home/3-Home-DID-DineintheDark-PhnomPenh.jpg" 
                  size="flex-image" 
                  alt='Dining at DiD'
                />
              </div>
              <div className="row col-xs-12 spacer-p-1 spacer-l-2" />
          </div>
        );
      default:
        return (
          <div id="home-page" className="text-purple text-left animated fadeIn">
            <div className="col-xs-12 row">
              <div className="col-xs-4" />
            </div>
            <span className="col-xs-12 no-padding">
              <h2 className="did-title col-xs-12 no-padding">DID - Dine in the Dark</h2>
            </span>

            <p>Welcome to DID - Dine in the Dark at the Sheraton Grande Sukhumvit,
           a Luxury Collection Hotel, Bangkok. Prepare yourself to embark on a journey of
           sensory awareness served by the visually impaired. </p>
            <p> DID - Dine in the Dark is a fine dining restaurant in which there are no lights inside.
           We offer you the chance to experience Asian, Western, or vegetarian surprise menus without
           seeing them, thereby heightening your non-visual senses in a playful and elegant way.
          </p>
            <p>
          All members of our service team are visually impaired and fluent in Thai and English.
          Our restaurant offers you a unique opportunity to discover their professional skills and
          support their employment in a sustainable way.
          </p>
            <p>
           Switch off the lights and switch on your senses; DID - Dine in the Dark is open
           from 18.00 hours, Tuesdays to Saturdays (last seating at 21.30 hours). For reservations,
           please call <a href="tel:+66026498358"> +66 (0)2 649 8358</a>,
           email <a href="mailto:bangkok@didexperience.com">bangkok@didexperience.com </a>
          or complete the online form provided in the <a href='javascript:void(0)' className="c-hand" onClick={() => changeSection('Reservations')}> Reservations</a> section of this website.
          </p>

            <div className="row col-xs-12 spacer-p-1 spacer-l-2" />
            <div className="col-xs-12 no-padding flex-container">
              <Modal_Image
                url="PhonmPenh/DID-PP-WS-09.jpg" size="flex-image"
              />
              <Modal_Image
                url="PhonmPenh/DID-PP-WS-10.jpg" size="flex-image"
              />
              <Modal_Image
                url="PhonmPenh/DID-PP-WS-11.jpg" size="flex-image"
              />
            </div>
          </div>
        );
    }
  }
};

exports.Experience_Page = class ExpPage extends ImageWaiter {
  constructor(props){
    super(props);
    this.state = { loaded: 0, start: Date.now() };
  }
  componentDidMount() {
    sendFBevent('ViewContent', { content_name: 'experience' });
  }
  render() {
    return (
      <div id="experience-page" className="text-purple text-left animated slideInUp">
        <span className="col-xs-12 no-padding">
          <div className="col-xs-4" />
          <h2 className="did-title col-xs-12 no-padding">Experience</h2>
        </span>
        <p>
        Indulge in an enjoyable culinary guessing game by discovering mouth-watering surprise 
        dishes in a pitch dark environment. Our restaurant is the perfect place for couples, 
        friends, and groups to experience a variety of cuisines and the local culture in a whole new way.
        </p>
        <p>
        In the lit welcome lounge, you can select a 4-course surprise menu and may also mention 
        to the host any dietary requirements. You can also order beverages, such as wines and 
        flavoursome surprise cocktails.
        </p>
        <p>
        You are then introduced to your visually impaired guide for the evening and led into the 
        dark dining room. At your table, the dinner and beverages are served in pitch darkness. 
        And at the end of the dinner, back in the lit lounge area, our host reveals and shows you 
        samples of the dishes. You are sure to be positively surprised!
        </p>

        <div className="row col-xs-12 spacer-l-2 spacer-p-1" />
        <div id="experience_gallery" className="col-xs-12 no-padding flex-container">
          <Modal_Image
            url="PhonmPenh/Experience/1-Experience-DID-DineintheDark-PhnomPenh.jpg"
            size="flex-image" callback={this.loadCallback}
          />
          <Modal_Image
            url="PhonmPenh/Experience/2-Experience-DID-DineintheDark-PhnomPenh.jpg"
            size="flex-image" callback={this.loadCallback}
          />
          <Modal_Image
            url="PhonmPenh/Experience/3-Experience-DID-DineintheDark-Bangkok.jpg"
            size="flex-image" callback={this.loadCallback}
          />
        </div>
        <div className="row col-xs-12 spacer-l-2 spacer-p-1" />
      </div>
    );
  }
};

const cuisine_intro = () =>
  (
    <p>
Enhance your non-visual senses by dining in complete darkness.
    </p>
  );


const cuisine_bkk = () =>
  (
    <span>
      {cuisine_intro()}
      <p>
      We offer a choice of four 4-course surprise menus, i.e. Asian, Western, Vegetarian, and the 
      Chef’s Selection. All dishes offer a surprising selection of flavours and textures designed 
      by our experienced team and the Sheraton Grande Sukhumvit’s award-winning chefs. We also 
      prepare vegan and gluten-free menus upon request, and accommodate dietary requirement in case of mild allergies.
      </p>
      <p>
      You may also opt for the wine-pairing option; each of your dishes will then be accompanied 
      by wines carefully chosen by our sommelier to further enhance your gourmet evening.
      </p>
      <p>
      All menus change every three months. Come back to always be surprised again.
      </p>
    </span>
  );


const cuisine_phnom_penh = () =>
  (
    <span>
      {cuisine_intro()}
      <p>
      We offer a choice of four 3-course surprise menus, i.e. Khmer, International, Vegetarian, and the Chef’s Monthly Selection. All dishes offers a surprising selection of tastes and textures. They are carefully and jointly designed by our experienced team and Michelin-starred chef Nick Medhurst. We also prepare vegan and gluten-free menus upon request, and accommodate dietary requirement in case of mild allergies.
     </p>
    </span>
  );


exports.Cuisine_Page = class CuisinePage extends ImageWaiter {
  constructor(props){
    super(props);
    this.state = { loaded: 0, start: Date.now() };
  }
  componentDidMount() {
    sendFBevent('ViewContent', { content_name: 'Cuisine' });
  }
  render() {
    return (
      <div id="cuisine-page" className={this.getClasses(this.state.loaded)}>
        <span className="col-xs-12 no-padding">
          <div className="col-xs-4" /> 
          <h2 className="did-title col-xs-12 no-padding">Cuisine</h2>
        </span>
        { this.props.location === 1 ? cuisine_bkk() : cuisine_phnom_penh() }
        <div className="row col-xs-12 spacer-l-2 spacer-p-1" />
        <div className="col-xs-12 no-padding flex-container">
          <Modal_Image
            url={this.props.location == 1 ? 'Bangkok/Cuisine/one.jpg' :
         'PhonmPenh/Cuisine/1-Main-DID-DineintheDark-PhnomPenh.jpg'} 
            size="flex-image"
          />
          <Modal_Image
            url={this.props.location == 1 ? 'Bangkok/Cuisine/two.jpg' :
         'PhonmPenh/Cuisine/2-Main-DID-DineintheDark-PhnomPenh.jpg'} 
            size="flex-image"
          />
          <Modal_Image
            url={this.props.location == 1 ? 'Bangkok/Cuisine/three.jpg' :
         'PhonmPenh/Cuisine/3-Main-DID-DineintheDark-PhnomPenh.jpg'} 
            size="flex-image"
          />
        </div>
        <div className="row col-xs-12 spacer-l-4 spacer-p-2" />
      </div>
    );
  }
};

exports.Purpose_Page = class PurposePage extends React.PureComponent {
  constructor(props){
    super(props);
  }
  componentDidMount() {
    sendFBevent('ViewContent', { content_name: 'purpose' });
  }
  render() {
    return (
      <div id="purpose-page" className="text-purple text-left animated slideInUp">
        <span className="col-xs-12 no-padding">
          <div className="col-xs-4" /> 
          <h2 className="did-title col-xs-12 no-padding">Purpose</h2>
        </span>
        <p>
        DID – Dine in the Dark embeds a social purpose at the core of its business model:
        </p>
        <ul>
          <li>
          Raising awareness: Through a role-reversal by which the visually impaired are the guides 
          in a pitch-dark environment, guests have a hands-on experience of visual impairment while 
          discovering the professional skills of a talented and enthusiastic workforce.
          </li>
          <li> 
          Empowerment: We build the confidence of the visually impaired and train them to take on 
          a fast-paced front-of-the-house role to showcase their hospitality and language skills.
          </li>
          <li> 
          Employment: We offer the visually impaired well-remunerated employment opportunities 
          which are otherwise all too rare in {this.props.location === 1 ? 'Thailand' : 'Cambodia'}.
          </li>
        </ul>
        <p> 
        We hope that other employers across all industries will recognize that the visually 
        impaired are a valuable workforce and will offer them an opportunity to integrate their team.
       </p>
        <div className="row col-xs-12 spacer-p-1 spacer-l-2" />
        <div className="col-xs-12 no-padding flex-container">
          <Modal_Image url={ this.props.location === 1 ? 
            'Bangkok/SocialPurpose/one.jpg' : 
            'PhonmPenh/SocialPurpose/1-SocialPurpose-DID-DineintheDark-PhnomPenh.jpg'} 
            size='flex-image' />
          <Modal_Image url={ this.props.location === 1 ? 
            'Bangkok/SocialPurpose/two.jpg' : 
            'PhonmPenh/SocialPurpose/2-SocialPurpose-DID-DineintheDark-PhnomPenh.jpg'} 
            size='flex-image' />
          <Modal_Image url={this.props.location === 1 ? 
            'Bangkok/SocialPurpose/three.jpg' :
            'PhonmPenh/SocialPurpose/3-SocialPurpose-DID-DineintheDark-PhnomPenh.jpg'} 
            size="flex-image" />
        </div>
        <div className="row col-xs-12 spacer-p-1 spacer-l-2" />
      </div>
    );
  }
};

const EventItem = () => {
    return (
      <div className='col-xs-12 event_item event_box'>
        <div className='align-center'>
          <Modal_Image 
            url={this.props.img} 
            className='img-responsive'
            size='col-xs-4 col-md-4'
          />
        </div>
        <div className='col-xs-8 text-left'>
          <span className='text-italic col-xs-12'>{this.props.date}</span>
          <h4 className='col-xs-12 text-bold'>{this.props.title}</h4>
          <p className='col-xs-12'>{this.props.text}</p>
        </div>
        <div className='row spacer-l-2 spacer-p-1' />
      </div>) 
}

const thailand_event_space = () => 
(
  <span>
  You can also make use of the chic adjacent BarSu lounge and bar area for larger group outings 
  and functions. 
  </span>
);

const cambodia_event_space = () =>
(
  <span>
  You can also make use of the lounge, bar and beautiful garden areas on the ground floor of our 
  typical French colonial ‘Compartiment Français’ shop house for larger group outings and functions. 
  </span>
);

exports.Events_Page = class EventsPage extends React.PureComponent {
  constructor(props){
    super(props);
  }
  componentDidMount() {
    sendFBevent('ViewContent', { content_name: 'events' });
  }
  render() {
    return (
      <div id="events-page" 
        className="text-purple text-left animated slideInUp col-xs-12">
        <span className="col-xs-12 no-padding">
          <div className="col-xs-4" /> 
          <h2 className="did-title col-xs-12 no-padding">Events</h2>
        </span>
        <p>
        We regularly host events at DID – Dine in the Dark. Ranging from seasonal celebrations to 
        wine tasting and live music performances, there is always something new and exciting to 
        experience in the dark. Ask us about upcoming events when making a reservation.
        </p>
        <p> 
        Our restaurant is also the perfect place for celebrating birthdays, corporate outings, 
        team-building, product launches and media events. We design memorable tailor-made events 
        in creative ways adapted to your needs and objectives.
        { this.props.location === 1 ? thailand_event_space() : cambodia_event_space() }
        The space can be transformed with visuals,
        live music, special activities and entertainment to fit your desired experience. 
        Initiate a discussion with us by using the 
        <a href='javascript:void(0)' className="c-hand" onClick={() => changeSection('contactus')}> Contact us form </a> 
        on this website.
        </p>
        <div className="row col-xs-12 spacer-p-1 spacer-l-2" />
        <div className="col-xs-12 no-padding flex-container">
          <Modal_Image url={ this.props.location === 1 ? 
            'Bangkok/Events/one.jpg' : 
            'PhonmPenh/Events/1-Events-DID-DineintheDark-PhnomPenh.jpg'} 
            size='flex-image' />
          <Modal_Image url={ this.props.location === 1 ? 
            'Bangkok/Events/two.jpg' : 
            'PhonmPenh/Events/2-Events-DID-DineintheDark-PhnomPenh.jpg'} 
            size='flex-image' />
          <Modal_Image url={this.props.location === 1 ? 
            'Bangkok/Events/three.jpg' :
            'PhonmPenh/Events/3-Events-DID-DineintheDark-PhnomPenh.jpg'} 
            size="flex-image" />
        </div>
        <div className="row col-xs-12 spacer-p-1 spacer-l-2" />
       </div>
    );
  }
};

const BKK_Media = () => {
    return (
      <div id="media-page" className="text-purple text-left row">
        <span className="col-xs-12 no-padding">
          <div className="col-xs-4" />
        </span>
        <div className='col-xs-12'>
          <h2 className="did-title col-xs-12 no-padding">Gallery</h2>
        </div>
        <div className="row col-xs-12 spacer-l-2 spacer-p-1" />
          <YouTubeEmbed
            width="300"
            height="300"
            YouTubeLink="https://www.youtube.com/embed/_bQwidTK11s"
          />
        <div className="row col-xs-12 spacer-l-2 spacer-p-1" />
      </div>
    );
}

const PhnomPenh_Media = () => {
    return (
      <div id="media-page" className="text-purple text-left animated fadeIn row">
        <div className='col-xs-12'>
          <h2 className="did-title col-xs-12 no-padding">Gallery</h2>
        </div>
        <div className="row col-xs-12 spacer-l-2 spacer-p-1" />
        <div className="row col-xs-10 col-xs-offset-1">
          <FacebookAlbum loc="2" id="968664903170052" />
          <FacebookAlbum loc="2" id="736264236410121" />
          <FacebookAlbum loc="2" id="585526901483856" />
          <FacebookAlbum loc="2" id="605127106190502" />
        </div>

        <div className="row col-xs-12 spacer-l-2 spacer-p-1" />
      </div>

    );
}


exports.Media_Page = class MediaPage extends React.PureComponent {
  constructor(props){
    super(props);
  }
  componentDidMount() {
    sendFBevent('ViewContent', { content_name: 'media' });
  }
  render() {
	  if (this.props.location == 1) {
	    return (<BKK_Media />);
	  }

	    return (<PhnomPenh_Media />);
  }
};


exports.Press_Page = class PressPage extends ImageWaiter {
  constructor(props){
    super(props);
    this.state = { loaded: 0, start: Date.now() };
  }
  shouldComponentUpdate(next_props, next_state) {
    if (next_state.loaded == 6 && this.state != next_state) {
      this.setState(this.state);
    }

    return false;
  }
  getDiDLink(loc) {
    if (loc === 1) {
      return 'https://www.tripadvisor.com/Restaurant_Review-g293916-d7365418-Reviews-DID_Dine_in_the_Dark-Bangkok.html';
    }
    return 'https://www.tripadvisor.com/Restaurant_Review-g293940-d5123195-Reviews-DID_Dine_in_the_Dark-Phnom_Penh.html';
  }
  componentDidMount() {
    sendFBevent('ViewContent', { content_name: 'press' });
  } 
  render() {
    return (
      <div id="press-page" className={this.getClasses(this.state.loaded)}>
        <span className="col-xs-12 no-padding">
          <div className="col-xs-4" />
          <h2 className="did-title col-xs-12 no-padding">Reviews</h2>
        </span>
        <p>
        Our unique concept and social business model regularly attracts international and local 
        media reviewing our restaurant. Please use the 
        <a href='javascript:void(0)' className="c-hand" onClick={() => changeSection('Contact us')}> Contact us </a> 
        form on this website if you would like to write a story, interview our team or learn 
        more about our successes in achieving our social mission. 
        </p>
        <p> 
        We also regularly receive excellent reviews from our guests.
        To see how others rate their experience with us, please visit the
        TripAdvisor page by <a href={this.getDiDLink(this.props.location)}> clicking here.</a> 
        </p>
        <div className="row col-xs-12 spacer-l-2 spacer-p-1" />
      </div>
    );
  }
};

