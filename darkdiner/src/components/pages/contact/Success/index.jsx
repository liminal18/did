import React from 'react';
import { store } from 'Redux/store';

const Contact_Success = () => {
  const changeSection = (section) => {
    if (section) { store.dispatch({ type: 'CHANGE_SECTION', section }); }
  };
    return (
      <div>
        <div className='col-xs-12 spacer-p-2 spacer-l-2' />
        <i className="fa fa-check-circle fa-5x" aria-hidden="true"></i>
        <h1>Thanks for Contacting Us!</h1>
        <p>Your message has been recieved</p>
        <button 
          className='col-xs-6 col-xs-offset-3 btn btn-primary' 
          onClick={() => changeSection('home')}>
          Return
        </button>
        <div className='col-xs-12 spacer-p-2 spacer-l-2' />
      </div> 
    );
};

export default Contact_Success;
