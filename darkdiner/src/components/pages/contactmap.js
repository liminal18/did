import React from 'react';
import { sendGaEvent, sendFBevent } from 'Services/analytics';
import { store } from 'Redux/store';
import GoogleMap from '../embed/googleMap';
import { validEmail, validPhone } from 'Services/validators';
import PostToApi from 'Services/ajax.js';
import Loader from 'Components/Loader/';
import Contact_Success from './contact/Success/';
import Error from 'Components/Error/';

const Contact_Page = class ContactPage extends React.Component {
  constructor(props){
    super(props);
    this.state = { 
      error: {}, 
      success: false, 
      loading: false, 
      name: '', 
      phone: '',
      email: '',
      message: ''
    };
  }
  changeSection(section) {
    if (section) { store.dispatch({ type: 'CHANGE_SECTION', section }); }
  }
  componentDidMount() {
    sendFBevent('ViewContent', { content_name: 'contact' });
  }
  addToState(k, el) {
    const value = el.target.value;
    this.state[k] = value;
    this.state.error[k] = false;
    this.setState(this.state);
  }
  getPhone(loc) {
    if (loc === 1) {
      return (
        <a title='Bangkok Phone Number' href="tel:+66(0)26498358"> +66 (0) 2649 8358</a>
      );
    }
    return (
      <a title='Phnom Penh Phone Number' href="tel:+855(0)77859458"> +855 (0)77 859 458</a>
    );
  } 
  validate() {
    this.state.error = {};
    if (!this.state.name) {
      this.state.error.name = true;
    }
    if (!this.state.phone) {
      this.state.error.phone = true;
    }
    if (!this.state.email) {
      this.state.error.email = true;
    }
    //phone is not required but we should show validation anyways
    const keys = Object.keys(this.state.error).
      filter((k) => k != 'phone');
    if (keys.length < 1) {
      return true;
    }
    this.setState(this.state);
    return false;
  }
  submitCallback(response, success) {
   this.state.loading = false; 
   if (success) {
      this.state.success = true;
      this.setState(this.state);
      return sendFBevent('Lead');
   }
   this.state.error.submit = response.responseJSON ? response.responseJSON : true;
   this.setState(this.state);
  }
  changeSuccess() {
    this.state.success = !this.state.success;
    this.setState(this.state);
  }
  submit(loc, submitCallback) {
    if (!this.validate()) { return false; }
    this.setState({loading: true});
    PostToApi(`/contact?loc=${loc}`, '#contactForm', submitCallback);
  }
  render() {
    if (this.state.success) {
      return (
        <Contact_Success back={this.changeSuccess.bind(this) } />
      );
    }
    if(this.state.loading){
      return(
        <Loader />
      );
    }

    return (
      <div
        id="contact-page"
        className="text-purple text-center col-xs-12 animated slideInUp"
      >
        <h2>Contact us</h2>
        <p className="row col-xs-12">
          For online reservations, you may find it easier to follow the
          <a
            title='Reservations Section'
            className="c-hand highlight"
            onClick={() => this.changeSection('Reservations')}
          > Reservations link </a>
          <span>
            available on the left sidebar of this website and follow the instructions thereafter.
          </span>
           For any other urgent enquiries, please contact us by phone at
          { this.getPhone(this.props.location) }.
        </p>
        <form onSubmit={(e) => {
              this.submit(this.props.location, this.submitCallback.bind(this));
              e.preventDefault();
           }} id="contactForm" className="form col-xs-12 form-validation">
          {this.state.error.submit ? <Error errors={this.state.error.submit} /> : null}
          <label htmlFor="name" className="col-xs-10 col-xs-offset-1">Your Name
            <input
              id="name" name="name" type="text" className="form-control" value={this.state.name}
              onChange={this.addToState.bind(this, 'name')} autoComplete='name'
            />
            { this.state.error.name ? <div className="text-danger">Name Required</div> : null }
          </label>
          <label htmlFor="email" className="col-xs-10 col-xs-offset-1">Your e-mail address
            <input
              id="email" name="email" pattern={validEmail} type="email" className="form-control"
              onChange={this.addToState.bind(this, 'email')} value={this.state.email} autoComplete='email'
            />
            { this.state.error.email ? <div className="text-danger">Email Required</div> : null }
          </label>
          <label htmlFor="phone" className="col-xs-10 col-xs-offset-1">Your Phone
            <input id="phone" name="phone" 
              minLength={3} maxLength={20} 
              type="tel" className="form-control"
              onChange={this.addToState.bind(this, 'phone')}
              value={this.state.phone} 
              autoComplete='tel'
            />
            { this.state.error.phone ?
              <div className="text-danger">Phone Number Suggested</div> : null
            }
          </label>
          <label htmlFor="message" className="col-xs-10 col-xs-offset-1">Your Message
            <textarea
              id="message" name="message" type="text" className="form-control"
              onChange={this.addToState.bind(this, 'message')} value={this.state.message}
            />
            { this.state.error.message ? <div className="text-danger">Invalid Message</div> : null }
          </label>
          <button
            className="btn btn-default col-xs-4 col-xs-offset-4" type="submit" 
          >
          Submit </button>
        </form>
        <div className='col-xs-12 spacer-p-2 spacer-l-2' />
      </div>
    );
  }
};

const Directions_Page = class DirectionsPage extends React.PureComponent {
  constructor(props){
    super(props);
  }
  shouldComponentUpdate(next_props, _) {
    return next_props.location != this.props.location;
  }
  getFooter(loc) {
    if (loc == 1) {
      return (
        <div className="col-xs-12" style={{ lineHeight: 'normal' }}>
          <p>DID - Dine in the Dark is located very centrally in Bangkok and easily accessible by car or taxi from Sukhumvit Road
        and Asoke (i.e. Sukhumvit Soi 21). Parking is available at the Sheraton Grande Sukhumvit.
       </p>
          <p>The closest BTS station is Asoke (walk towards, and pass by, BTS Exit 2) and the closest MRT station is Sukhumvit (MRT Exit 3).
       There is a direct sky-walk pedestrian access from those stations to the lobby area of the Sheraton Grande Sukhumvit. </p>
        </div>
      );
    }
    return (
      <div className="col-xs-12" style={{ lineHeight: 'normal' }}>
        <p>DID - Dine in the Dark is located very centrally in Phnom Penh and easily accessible by taxi, tuk tuk or by foot from the Royal Palace and the riverside quays. Parking is also available in a side street nearby if you come with your own car; speak with a member of our team upon arrival.
       </p>
      </div>
    );
  }
  getHeader(loc) {
    if (loc == 1) {
      return (
        <span>
          <div className="col-xs-12 spacer-p-1" />
          <h2>DID - Dine in the Dark: Bangkok branch</h2>
          <h4>Located at the Sheraton Grande Sukhumvit
            · 250 Sukhumvit Road · Bangkok · Thailand</h4>
        </span>

      );
    }
    if (loc == 2) {
      return (
        <span>
          <div className="col-xs-12 spacer-p-1" />
          <h2>DID - Dine in the Dark: Phnom Penh branch</h2>
          <h4>Located at 126, Street 19 |
            Cross streets 154 & 172 Phnom Penh, Cambodia</h4>
        </span>
      );
    }

    return (
      <span>
        <div className="col-xs-12 spacer-p-1" />
        <h2>DID - Dine in the Dark: Bangkok branch</h2>
        <h4>Located at the Sheraton Grande Sukhumvit
            · 250 Sukhumvit Road · Bangkok · Thailand</h4>
        <p><a title='Bangkok Phone Number' href="tel:+66026498358">Phone: +66 (0) 2649 8358</a></p>
        <p>Fax: +66 (0) 2649 8000</p>
        <p><a title='Bangkok Email' href="mailto:info@didexperience.com">
            Email: info@didexperience.com
          </a></p>
        <a title='Bangkok Facebook' href="http://www.facebook.com/DIDBangkok">
            Facebook: www.facebook.com/DIDBangkok
          </a>
        <p>Twitter: @DIDexperience</p>
      </span>
    );
  }
  render() {
    return (
      <div id="directions-page" className="text-purple text-center animated slideInRight fullHeight">
        {this.getHeader(this.props.location)}
        <GoogleMap location={this.props.location} />
        <div className="col-xs-12 spacer-p-1 spacer-l-2" />
        {this.getFooter(this.props.location)}
        <div className='col-xs-12 spacer-p-2 spacer-l-2' />
      </div>
    );
  }
};

/** *
* ContactOrMap
*/
module.exports = class ContactOrMap extends React.PureComponent {
  constructor(props){
    super(props);
    this.state = { view: 1 };
  }
  changeSection(sec) {
    return () => this.setState({ view: sec });
  }
  displaySection(view) {
    if (view == 1) {
      return (
        <Contact_Page location={this.props.location} />
      );
    }

    return (
      <Directions_Page location={this.props.location} />
    );
  }
  getClasses(sec, extraClass) {
    const classes = 'col-xs-6 ' + extraClass + ' ';
    if (this.state.view == sec) {
      return `${classes}c-hand highlight tab selected`;
    }

    return `${classes}c-hand highlight tab`;
  }
  render() {
    return (
      <div className="col-xs-12 no-padding fullHeight">
        <div className="tab-bar">
          <ul className="tabby col-xs-12 no-padding text-purple">
            <li className={this.getClasses(1, 'left-tab')} onClick={this.changeSection(1)}><h3>Contact us</h3></li>
            <li className={this.getClasses(2, 'right-tab')} onClick={this.changeSection(2)}><h3>Map</h3></li>
          </ul>
        </div>
        <div className="col-xs-12 fullHeight">
          {this.displaySection(this.state.view)}
        </div>
      </div>
    );
  }
};
