import React from 'react';
import DIDFooter from 'Components/Footer';
import renderer from 'react-test-renderer';
import jQuery from 'jquery';
global.jQuery = jQuery;

global.window = {};
global.window.frames = {};
global.window.frames.tripadvisor = {};
global.window.frames.tripadvisor.document = {};
global.document = { };
global.document.getElementById = function(selector){
  return {contentWindow: {} }
}

it('stage should render correctly', () => {
  const stage = renderer.create(<DIDFooter />).toJSON();
  expect(stage).toMatchSnapshot();
})
