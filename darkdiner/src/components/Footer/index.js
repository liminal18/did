import React from 'react';
import { store } from 'Redux/store';
import { TripAdvisorIframes } from 'Components/embed/tripadvisor';
import { FacebookLikeIframe } from 'Components/embed/facebook';
import { sendFBevent } from 'Services/analytics.js';
/**
* DiDFooter
*/

export default class DiDFooter extends React.PureComponent {
  constructor(props){
    super(props);
  }
  shouldComponentUpdate(next_props) {
    return next_props.location != this.props.location
  }
  changeLocation(loc) {
    store.dispatch({ type: 'GO_TO_LOCATION', loc });
    store.dispatch({ type: 'CHANGE_SECTION', section: 'Home' });
    sendFBevent('changedlocation',
          { content_name: loc === 1 ?
            'Bangkok' : 'PhnomPenh' }, true);
  }
  changeSection(sec) {
    store.dispatch({ type: 'CHANGE_SECTION', section: sec });
  }
  getHref(loc) {
    switch (loc) {
      case 1:
        return 'https://www.facebook.com/DIDBangkok';
      case 2:
        return 'https://www.facebook.com/DIDPhnomPenh';
      default:
        return 'https://www.facebook.com/DIDBangkok';
    }
  }
  render() {
    return (
      <footer id='footer' 
        className='text-purple layer2 col-xs-12 solidify flex-total-center'>
          <div id='footer-left' className='flex-total-center flex-1'>
            <span 
              className='flex-1 flex-text flex-space-around highlight c-hand'
              onClick={() => this.changeLocation(1)}>Bangkok
            </span>
            <span 
              className='flex-2 flex-text flex-space-around highlight c-hand'
              onClick={() => this.changeLocation(2)}>Phnom Penh
            </span>
            <span 
              className='flex-1 flex-text flex-space-around highlight c-hand'
              onClick={() => this.changeSection('FAQ')}>FAQ
            </span>
            <span 
              className='flex-1 flex-text flex-space-around highlight c-hand'
              onClick={() => this.changeSection('Franchise')}>Franchise
            </span>
          </div>
          <div id='footer-center' style={{flexGrow: 4, flexShrink: 6}} />
          <div id='footer-right' className='flex-total-center flex-1'>
              <a name='facebook-icon' 
                 href={this.getHref(this.props.location)}
                 title='DiD @ Facebook' 
                 className='flex-1 flex-icon flex-space-around'>
                <img 
                  alt='facebook icon' 
                  src='images/icons/icon-fb.png' 
                  className='footer-icon' 
                />
              </a>
              <a 
                 name='link-to-twitter'
                 title='DiD @ Twitter' 
                 href='https://twitter.com/didexperience' 
                 className='flex-1 flex-icon flex-space-around'>
                <img 
                  alt='twitter icon' 
                  src='images/icons/icon-twitter.png' 
                  className='footer-icon' 
                />
              </a>
              <a href={
                this.props.location == 1 ? 
                'mailto:bangkok@DIDexperience.com' : 'mailto:phnompenh@DIDexperience.com'}
                title='DID Email'
                name='DID Email' 
              className='flex-1 flex-icon flex-space-around'>
                <img 
                  alt='email icon' 
                  src='images/icons/icon-mail.png' 
                  className='footer-icon' 
                />
              </a>
            
              <div className='flex-total-center flex-space-around flex-1'> 
                <TripAdvisorIframes 
                  width='6rem' 
                  height='1.5rem' 
                  location={this.props.location} 
                />
              </div>
              <div className='flex-1 flex-space-around flex-total-center'>
                <FacebookLikeIframe location={this.props.location} />
              </div>
          </div> 
      </footer>
    );
  }
};
