const React = require('react');
const { isMobile, isAndroid } = require('Services/mobile');

/**
* BackGround
*/
 // interval for changing photos
const interval = 4000;

export default class Background extends React.PureComponent {
  constructor(props){
    super(props);
    this.changeInterval = null;
    this.state = { image: 0 }
  } 
  locDir() { 
    return this.props.location == 1 ? 'Bangkok/' : 'PhnomPenh/'; 
  }
  makePhotoArray(location) {
    return [1, 2, 3, 4].map(this.makeUrls.bind(this));
  }
  makeUrls(i, _) {
    return `images/background/${this.locDir()}${i}.jpg`;
  }
  componentWillUpdate(next_props){
    if(this.props.location !== next_props.location){
      clearInterval(this.changeInterval); 
      if (!isMobile) {
        this.changeInterval = setInterval(this.changePhoto.bind(this), interval);
        this.state.image = 0;
      }  
    }
  }
  componentDidMount() {
    if (!isMobile) {
      this.changeInterval = setInterval(this.changePhoto.bind(this), interval);
      this.setState(this.state);
    }
  }
  componentWillUnmount() {
    clearInterval(this.changeInterval);
  }
  changePhoto(imageIndex) {
    const update = this.makePhotoArray(this.props.location);
    let next = 0;
    if (!update) { return false; }
    if (this.state.image >= 
      this.makePhotoArray(this.props.location).length) 
      { next = 0; } 
    else { next = this.state.image + 1; }
    this.setState({image: next });
  }
  getIndexes(){
    const photos = this.makePhotoArray(this.props.location);
    const nextIndex = this.state.image + 1 >= photos.length ? 
      0 : this.state.image + 1;
    return [this.state.image, nextIndex];
  }
  getClasses(index){
    const [currentIndex, nextIndex] = this.getIndexes();
    if(currentIndex === index){
      return "bgImg bgFade";
    }
    if(nextIndex === index){
      return "bgImg solid";
    }
    return "bgImg transparent";
  }
  makeImg() {   
    return(<img 
               id='bgImg_1'
               alt='img-bg' 
               className='bgImg solid' 
               src={'images/background/' + this.locDir() + '1.jpg'} >
           </img>
   );
  }
  makeImages(url, index){
    return(
      <img 
        key={'img_' + index}
        alt='DiD Background' 
        className={this.getClasses(index)}
        src={url}>
      </img>)
  }
  getBackground(){
    if(isMobile){
      return this.makeImg();
    }
   return this.makePhotoArray(this.props.location)
            .map(this.makeImages.bind(this));
  }
  render() {
    return (
      <div 
        id='bg' 
        className='col-xs-12 text-center no-padding sublayer bg-gradient'
      >
          {this.getBackground()}
      </div>
     );
  }
}
