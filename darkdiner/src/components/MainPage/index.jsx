import React from 'react';
import LightSwitch from 'Components/LightSwitch/';
import Menu_Items from 'Components/Menu/Items/';
import SectionPicker from 'Components/SectionPicker/';
import DiDFooter from 'Components/Footer/';
import { sendGaEvent, sendFBevent } from 'Services/analytics/';
import { connect } from 'react-redux';

/***
* MainPage
*/

const NavSideMenu = function(props){
  return(
    <div className='nav-side-menu no-mobile-padding'>
      <div id='sideMenu' className='panel col-md-12 col-xs-12 no-padding solidify'>
        <div className='spacer-l-1 spacer-p-1 row' />
        <div className='col-xs-10 col-xs-offset-1 align-center'>
          <img alt='DiD Logo' src='images/logo.png' className='center-block img-responsive' />
        </div>
        <div className='col-xs-12 spacer-l-3 spacer-p-3' />
        <Menu_Items location={props.location} />
      </div>
      <div className='row spacer-l-2 spacer-p-1' />
    </div>
  );
}

const MainHeader = function(props){
  return(
    <div className='col-md-3 col-xs-4  panel spacer-l-8 spacer-p-4 solidify'>
      <div className='flex-container flex-total-center'>
        <h4 className='text-white'>{props.section}</h4>
      </div>
    </div>
  );
}


class MainPage extends React.PureComponent {
  constructor(props){
    super(props);
  }
  componentDidMount() {
    sendFBevent('ViewContent', { content_name: 'main' });
  }
  render() {
    return(
    <div 
      id='MainPage' 
      className='col-xs-12 text-center MainPage no-padding animated fadeIn'>
      <div id='AboveFooter' className='col-xs-12'>
        <div id='top' className='row'>
          <div id='top-left' className='col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding'>
            { this.props.lights ? <LightSwitch 
                                         resize={this.props.resize} 
                                         lights={this.props.lights}
                                         cols='col-md-4 col-xs-4 no-padding'
                                       /> : <div className='col-xs-4 col-md-4 no-padding' />
            }
            <NavSideMenu location={this.props.location} />
          </div>
          <div id='main' className='col-xs-8 col-sm-8 col-md-7'>
            <div className='col-xs-12 spacer-l-15 no-padding'>
              <div className='col-md-9 col-xs-8' />
              <MainHeader section={this.props.section} />
            </div>
            <div 
              id='main-content' 
              className='panel col-xs-12 did-bg main-content text-purple no-padding solidify'>
              <SectionPicker section={this.props.section} loc={this.props.location} />
            </div>
          </div>
        </div>
        <div id='mainBottomSpacer' className='col-xs-12 spacer-p-6 spacer-l-6' />
      </div>
      <DiDFooter location={this.props.location} />
    </div>
   );
  }
}

const mapProps = (state, props) => ({ ...state });
const mapDispatch = (dispatch) => ({});
export default connect(mapProps, mapDispatch)(MainPage);
