import React from 'react';
import ReactDOM from 'react-dom';
import { store } from 'Redux/store';
import { isMobile } from 'Services/mobile';

/***
* tapable is a special component capable of detecting touches undermeath a z index
*/

export default class Tapable extends React.Component {
  constructor(props){
    super(props);
    this.state = { move: { touch: false, X: null, Y: null, start: null, end: null },
      touched: false,
      resize: store.getState().resize };
    // this is for storing the funcs we will use in event listeners
    this.methods = {
      tMove: null,
      tStart: null,
      tEnd: null,
      mMove: null,
      pClick: null,
      highlightInterval: null
    }
  }
  shouldComponentUpdate(nextProps, nextState) {
    return nextState.touched === this.state.touched; 
  }
  /**
  * register Box finds the clien rectangle of the current element
  * @ param {Object} l /> dom node
  */
  registerBox(l) {
    if (l) {
      return l.getClientRects()[0];
    }
    return null;
  }
  preClick(e) {
    this.checkClick(e, this.registerBox(ReactDOM.findDOMNode(this)));
  }
  checkClick(e, box) {
    if (e.clientX > box.left && e.clientX < box.right) {
      if (e.clientY > box.top && e.clientY < box.bottom) {
        e.stopPropagation();
        this.props.action();
        return true;
      }
    }
    return false;
  }
  checkTouch(touch, box, event) {
    this.state.move.touch = true;
    this.setState(this.state);
    if (this.state.move.X > (box.left - 5) && this.state.move.X < (box.right + 5)) {
      if (this.state.move.Y > (box.top - 10) && this.state.move.Y < (box.bottom + 20)) {
        const duration = this.state.move.end - this.state.move.start;
        if (duration > 0 && duration < 400) {
          if(event){
            event.stopPropagation();
          }
          this.props.action();
          return true;
        }
      }
    }
    return false;
  }
  touchStart(e) {
    e.preventDefault();
    if (e.touches.length >= 1) {
      this.setState({ move: { touch: true,
        X: e.touches[0].clientX,
        Y: e.touches[0].clientY,
        start: Date.now(),
        end: null } });
    } else {
      this.setState({ move: { touch: true, X: null, Y: null, start: null } });
    }
  }
  /**
  * touchEnd prevents the default behavoir 
  * and also let's us know how long the touch lasted
  * @param {Object} e the touch event
  */
  touchEnd(e) {
    e.preventDefault();
    const box = this.registerBox(ReactDOM.findDOMNode(this));
    const newMove = { move: { touch: true,
      X: this.state.move.X,
      Y: this.state.move.Y,
      start: this.state.move.start,
      end: Date.now() } };
    this.setState(newMove);
    this.checkTouch(newMove, box, e);
  }
  componentDidMount() {
    // speed up options for modern browsers
    const Event_Options = {
      capture: false,
      once: false,
      passive: false
    };
    //store the funcs so we can dispase of them later
    this.methods.tMove = this.touchMove.bind(this); 
    this.methods.tStart = this.touchStart.bind(this);
    this.methods.tEnd = this.touchEnd.bind(this);
    this.methods.mMove = this.mouseMove.bind(this);
    this.methods.pClick = this.preClick.bind(this);
    
    if (isMobile) {
      document.addEventListener('touchmove', this.methods.tMove, Event_Options);
      document.addEventListener('touchstart', this.methods.tStart, Event_Options);
      document.addEventListener('touchend', this.methods.tEnd, Event_Options);
    } else {
      document.addEventListener('mousemove', this.methods.mMove, Event_Options);
      document.addEventListener('click', this.methods.pClick, Event_Options);
    }
    this.methods.highlightInterval = setInterval(this.highlight.bind(this), 100);
  }
  componentWillUnmount() {
    const Remove_Options = {
      capture: false,
      passive: true
    }; 
    document.removeEventListener('touchmove', this.methods.tMove, Remove_Options);
    document.removeEventListener('touchstart', this.methods.tStart, Remove_Options);
    document.removeEventListener('touchend', this.methods.tEnd, Remove_Options);
    document.removeEventListener('mousemove', this.methods.mMove, Remove_Options);
    document.removeEventListener('click', this.methods.pClick, Remove_Options); 
    clearInterval(this.methods.highlightInterval);
  }
  mouseMove(e) {
    e.preventDefault();
    const move = { touch: this.state.move.touch,
      X: e.clientX,
      Y: e.clientY,
      start: null,
      end: this.state.move.end };
    this.collide(this.registerBox(ReactDOM.findDOMNode(this)), move, e);
  }
  touchMove(e) {
    e.preventDefault();
    const move = { touch: true,
      X: e.touches[0].clientX,
      Y: e.touches[0].clientY,
      start: this.state.move.start,
      end: this.state.move.end,
    };
    this.collide(this.registerBox(ReactDOM.findDOMNode(this)), move, e);
  }
  componentDidUpdate() {
    this.registerBox(ReactDOM.findDOMNode(this));
  }
  collide(box, mouse, event) {
    if (!box || !mouse) { 
      this.setState({touched: false}); 
      return false; 
    }
    if (mouse.X > box.left - 10 && mouse.X < box.right + 10) {
      if (mouse.Y > box.top && mouse.Y < box.bottom) {
        if (this.state.touched == false) {
          this.state.touched = true;
          this.setState(this.state);
        }
        if (!mouse.end) { 
          return false; 
        }
        const touchInterval = mouse.end - mouse.start;
        if (touchInterval > 400) { 
          return false; 
        }
        if(event){
          event.stopPropagation();
          // this is to stop the touch event from going further
        }
        return this.props.action();
      }
    } 
    this.state.touched = false;
    this.setState(this.state);
    return false;
  }
  hover(event) {
    this.setState({touched: true});
  }
  hoverEnd(event) {
    this.setState({touched: false});
  }
  highlight() {
    if (this.state.touched) {
      return '#ED75DA';
    }
    return '#b2a0b9';
  }
  click() {
    return this.props.action();
  }
  render() {
    return(
      <div
        role='button'
        tabIndex='-1' 
        className='tapable text-white c-hand'
        onMouseLeave={this.hoverEnd.bind(this)}
        ref={this.registerBox}
        style={{color: this.highlight.bind(this)() }}
      >
        {this.props.children}
      </div>
    )
 }
};
