import React from 'react';
import TestElement from './index.jsx';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer.create(
    <TestElement loc={1} section="home" />
  ).toJSON();
  expect(tree).toMatchSnapshot();
})
