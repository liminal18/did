import React from 'react';
import { Home_Page, Experience_Page, Cuisine_Page,
  Purpose_Page, Events_Page, Media_Page,
  Press_Page, ContactOrMap, Reservation_Page,
  FAQ_Page, Career_Page, Franchise_Page,
} from 'Components/pages/';

/***
* SectionPicker
*/

export default class SectionPicker extends React.PureComponent {
  constructor(props){
    super(props);
  }
  shouldComponentUpdate(next_props) {
    if (this.props.section != next_props.section) {
      return true;
    }
    if (this.props.loc != next_props.loc) {
      return true;
    }
    return false;
  }
  render() {
    const section = this.props.section.replace(/\s/g, '').toLowerCase();
    switch (section) {
      case 'home':
        return (<main className="col-xs-12">
          <Home_Page location={this.props.loc} />
        </main>);
      case 'experience':
        return (<main className="col-xs-12">
          <Experience_Page location={this.props.loc} />
        </main>);
      case 'cuisine':
        return (<main className="col-xs-12">
          <Cuisine_Page location={this.props.loc} />
        </main>);
      case 'purpose':
        return (<main className="col-xs-12">
          <Purpose_Page location={this.props.loc} />
        </main>);
      case 'events':
        return (<Events_Page location={this.props.loc} />);
      case 'media':
        return (<main className="col-xs-12">
          <Media_Page location={this.props.loc} />
        </main>);
      case 'reviews':
        return (<main className="col-xs-12">
          <Press_Page location={this.props.loc} />
        </main>);
      case 'contactus':
        return (<ContactOrMap location={this.props.loc} />);
      case 'reservations':
        return (<main className="col-xs-12">
          <Reservation_Page location={this.props.loc}  />
        </main>);
      case 'faq':
        return (<main className="col-xs-12">
          <FAQ_Page location={this.props.loc} />
        </main>);
      case 'careers':
        return (<main className="col-xs-12">
          <Career_Page location={this.props.loc} />
        </main>);
      case 'franchise':
        return (
          <main className="col-xs-12">
            <Franchise_Page location={this.props.loc} />
          </main>
        );
      default:
        return (<main className="col-xs-12">
          <Home_Page location={this.props.loc} />
        </main>);
    }
  }
};
