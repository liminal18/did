const React = require('react');
/** *
* YouTubeEmbed
*/
module.exports = class YouTubeVideo extends React.PureComponent {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <iframe
        ref='iframe'
        title='youtube'
        target='_parent' 
        height={this.props.height}
        width={this.props.width}      
        frameBorder={0}
        src={this.props.YouTubeLink}
        className='align-center col-xs-12'
        style= {{ maxWidth: '100%', maxHeight: '100%'}}
        allowFullScreen={true}
      />
      )}
};
