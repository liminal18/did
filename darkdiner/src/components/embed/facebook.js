const React = require('react');

exports.FacebookAlbum = require('./facebookAlbum');

/**
* facebookLikeIframe
* this is the FaceBook Iframe found in the footer
* @ param {Object} state
* @ param {{location: number}} props{location: number} the current location
*/
exports.FacebookLikeIframe = class FacebookLikeIframe extends React.PureComponent {
  constructor(props){
    super(props);
  }
  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.location == this.props.location) {
      return false;
    }

    return true;
  }
  geturl(loc) {
    switch (loc) {
      case 1:
        return 'https://www.facebook.com/DIDBangkok';
      case 2:
        return 'https://www.facebook.com/DIDPhnomPenh';
      default:
        return 'https://www.facebook.com/DIDBangkok';
    }
  }
  render() {
    return (
        <iframe src={`https://www.facebook.com/plugins/like.php?href=${this.geturl(this.props.location)}&width=89&layout=button_count&action=like&show_faces=false&share=false&height=21&appId=874616072650969`} 
          style={{border: 'none', overflow: 'hidden', width: '6rem', height: '1.5rem'}}
          scrolling='no'
          frameBorder='0'
          allowTransparency='true'
          title='facebook'
        />
     );
  }
};
