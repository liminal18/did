import React from 'react';
const { store } = require('../../redux/store');
const { minSort, maxSort, DateThenLikes } = require('../../Services/sort');
import Error from '../Error/index.jsx';

// makes a query string
const fbQuery = (loc, id, env) =>{
 const base = env === "PRODUCTION" ? 
   "//" + window.location.host + "/api" : "//127.0.0.1:8000";
 const query = `/facebook?action=album&loc=${loc}&id=${id}`;
 return base + query;
}
/** *
* class for the fb images which also sums up the album
* @ param {Object} fb_album the facebook album returned from the api
* @ param {Boolean} mobile are we on mobile
*/
class FBImage {
  constructor(fb_album, mobile) {
    this.id = fb_album.id || 0;
    this.name = fb_album.album.name || null;
    this.date = fb_album.updated_time || new Date();
    this.likes = this.getLikes(fb_album);
    this.image = this.getImage(fb_album.images, mobile);
  }
  getImage(images, mobile) {
    if (mobile) {
      return images.sort(minSort('height'))[0];
    }
    return images.sort(maxSort('height'))[0];
  }
  getLikes(album) {
    if (!album.likes || !album.likes.data) {
      return 0;
    }
    return album.likes.data.length;
  }
}


const COVER = 'COVER';
const ALBUM = 'ALBUM';

/***
* facebook Album
* takes a location and an album id and returns images for it
* params {number} location
* params {number} id
* params {boolean} mobile
*/

module.exports = class FaceBookAlbum extends React.Component {
  constructor(props){
    super(props);
    this.state = { images: [], loading: true, error: false, mode: COVER };
    this.getImages = this.getImages.bind(this);
  }
  // map the returned images array returning a single img for each img
  getImages(album) {
    return new FBImage(album, this.props.mobile);
  }
  success(response) {
   // take the images filter out the nulls and undefined
   // then setState with the results
    try{
      const json = JSON.parse(response);
      if(json.data && json.data.length > 0){
        this.setState({
          images: json.data.map(this.getImages)
           .filter((i) => { if (i) { return i; } }),
          loading: false,
          error: false,
          mode: COVER,
        });
      } else {
        this.setState({
          images: [],
          loading: false,
          error: true,
          mode: COVER
        });
      }
    } catch(err){
        console.error(err);
        this.setState({
          images: [],
          loading: false,
          error: true,
          mode: COVER
        }); 
    }
 }
  fail(error) {
    console.error(error);
    this.setState({
      images: [],
      loading: false,
      error: true,
      mode: COVER,
    });
  }
  componentWillMount() { 
    jQuery.ajax({
      url: fbQuery('2', this.props.id, process.env.NODE_ENV.toUpperCase()),
      method: 'GET',
      success: this.success.bind(this),
      error: this.fail.bind(this),
    });
  }
  render() {
    if (this.state.error) { 
      return(
        <Error />        
      ); 
    }
    if (this.state.loading) {
      return (
        <div
          id={`fb_album_${this.props.id}_loading`}
          className="col-xs-12"
        >
          <div className="col-xs-4" />
          <div className="loader col-xs-4" />
          <div className="col-xs-12 spacer-p-1 spacer-l-2" />
        </div>
      );
    }
    const firstImage = this.state.images.sort(DateThenLikes)[0];
    if (this.state.mode === COVER) {
      return (
        <div
          id={`fb_album_${this.props.id}_loading`}
          className="animated fadeIn col-xs-12 col-md-10 col-md-offset-1"
        >
          <h4 className='col-xs-12 gallery-title'></h4>
          <img
            src={firstImage.image.source}
            className="img-responsive album_cover c-hand"
            onClick={() => { this.state.mode = ALBUM; this.setState(this.state); }}
          />
          <button 
            onClick={() => { this.state.mode = ALBUM; this.setState(this.state); }}
            className='col-xs-12 btn btn-default btn-gallery'>
            <i className="fa fa-folder-open" aria-hidden="true"></i>
            <span> {firstImage.name || 'DiD Photos'}</span> 
          </button>
          <div className="col-xs-12 spacer-p-1 spacer-l-2" />
        </div>
      );
    } else
    return (
      <div id={`fb_album_${this.props.id}`} className="animated slideInLeft col-xs-12">
        <div className="col-xs-12 spacer-p-1 spacer-l-2" />
        <h4 className='col-xs-12 gallery-title'></h4>
        <div className='flex-row'>
        { this.state.images.sort(DateThenLikes).map((i, index) => (
          <img
            key={`fb_album_${this.props.id}_img_${index}`}
            onClick={
              () => store.dispatch({ type: 'SHOW_MODAL', src: i.image.source })}
            className="img-responsive c-hand gallery-image" src={i.image.source}
          />))
        }
        </div>
        <button 
            onClick={() => { this.state.mode = COVER; this.setState(this.state); }}
            className='col-xs-12 btn btn-default btn-gallery'> 
              <i className="fa fa-folder" aria-hidden="true"></i> 
              <span> - {firstImage.name || 'DiD Photos'}</span>
        </button>

        <div className="col-xs-12 spacer-p-1 spacer-l-2" />
      </div>
    );
  }
};
