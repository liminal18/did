const React = require('react');
/** *
* Google Map Component
*/
module.exports = class GoogleMap extends React.PureComponent {
  constructor(props){
    super(props);
    // to do get this from server
    this.key = 'AIzaSyAr_i6JG3Q63b2lYkB_1ALbEo8RbGK5OwQ';
  }
  shouldComponentUpdate(next_props) {
    return next_props.location != this.props.location;
  }
   // this is my key
  getUrl(loc) {
    if (loc == 1) {
      return 'https://www.google.com/maps/embed/v1/place?key=AIzaSyAr_i6JG3Q63b2lYkB_1ALbEo8RbGK5OwQ&q=Dine++in+the+Dark,Bangkok+Thailand';
    }
    if (loc == 2) {
      return 'https://www.google.com/maps/embed/v1/place?key=AIzaSyAr_i6JG3Q63b2lYkB_1ALbEo8RbGK5OwQ&q=Dine++in+the+Dark,Phnom+Penh+Cambodia';
    }

    return 'https://www.google.com/maps/embed/v1/place?key=AIzaSyAr_i6JG3Q63b2lYkB_1ALbEo8RbGK5OwQ&q=Dine++in+the+Dark,Bamgkok+Thailand';
  }
  render() {
    return (
      <iframe id="gmap" frameBorder="0" 
        style={{ border: 0, width: '90%', height: '35vh' }} 
        src={this.getUrl(this.props.location)} allowFullScreen />
    );
  }
};
