const React = require('react');

exports.TripAdvisorReviews = class TripAdvisorReviews extends React.PureComponent {
  constructor(props){
    super(props);
  }
  componentWillMount() {
    const script = document.createElement('script');
    script.src = 'https://www.jscache.com/wejs?wtype=socialButtonReviews&amp;uniq=523&amp;locationId=7365418&amp;color=green&amp;size=rect&amp;lang=en_US&amp;display_version=2';
    script.async = true;
    document.body.appendChild(script);
  }
  componentDidMount() {
    const functions = Object.getOwnPropertyNames(window);
    function currentInjectorFunction(name) {
      const injectoStub = /injectselfserveprop[0-9]+/;
      return injectoStub.test(name);
    }
    const currentInjectorName = functions.find(currentInjectorFunction);
    window[currentInjectorName]();
  }
  render() {
    return (
      <div>
        <div id='TA_selfserveprop574' className='TA_selfserveprop'>
          <ul id='YLbGK1' className='TA_links pJdjT7d'>
            <li id='cKUjW7' className='MEsRGT'>
              <a 
                name='tripadvisor_reviews' 
                title='Trip Advisor Reviews' 
                target='_blank' 
                href='https://www.tripadvisor.com/'
              >
                <img 
                  src='https://www.tripadvisor.com/img/cdsi/img2/branding/150_logo-11900-2.png'
                  alt='TripAdvisor' />
              </a>
            </li>
          </ul>
        </div>
      </div> 
    );
  }
};

exports.TripAdvisorIframes = class TripAdvisorIframes extends React.PureComponent {
  constructor(props){
    super(props);
  }
  shouldComponentUpdate(next_props) {
    return this.props.location != next_props.location;
  }
  getSrc(loc) {
    switch (loc) {
      case 1:
        return 'tripAdvisor/tripAdvisorBangkok.html';
      case 2:
        return 'tripAdvisor/tripAdvisorCambodia.html';
      default:
        return 'tripAdvisor/tripAdvisorBangkok.html';
    }
  }
  componentDidMount() {
    const tripAdvisorBody = jQuery('iframe#tripadvisor').contents().find('body');
    const tripAdvisorIframe = document.getElementById('tripadvisor');
    const iframeDocument = window.frames.tripadvisor.document;
    const iFrameContent = tripAdvisorIframe.contentWindow || tripAdvisorIframe.contentDocument.document || tripAdvisorIframe.contentDocument;
    tripAdvisorBody.attr('style', 'margin-top: 1.5%;');
    tripAdvisorBody.attr('id', 'tadvisorBody');
  }
  render() {
    return (
      <iframe name="tripadvisor" id="tripadvisor" title='tripadvisor' frameBorder="0" scrolling='no'
        style={{ border: 0, width: this.props.width, height: this.props.height }} src={this.getSrc.bind(this)(this.props.location)}       />
    );
  }
};
