import React from 'react';
import TestElement from './index.jsx';
import renderer from 'react-test-renderer';
import { store } from 'Redux/store';
import { Provider } from 'react-redux';

it('renders correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <TestElement />
    </Provider>
  ).toJSON();
  expect(tree).toMatchSnapshot();
})
