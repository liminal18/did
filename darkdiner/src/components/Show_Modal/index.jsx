import React from 'react';
const { isMobile } = require('Services/mobile');
const { sendGaEvent, sendFBevent } = require('Services/analytics');
import { connect } from 'react-redux';
import { ImageWaiter } from 'Components/mixins/';
import { Dissolver } from 'Components/Loader/';
/***
* Show_Modal
*/

class Show_Modal extends ImageWaiter {
  constructor(props){
    super(props);
  }
  componentDidUpdate() {
   if (this.props.url) {
      sendFBevent('ViewContent', 
        { content_name: `Modal for ${this.props.url}`, 
          value: this.props.show });
    }
  }
  transformURL(url) {
    // if the base url is not for the origin then return the url as is
    const fbURL = /fbcdn|facebook/;
    if (fbURL.test(url)) {
      return url;
    }
    return `images/${url}`;
  }
  render() {
    if (!this.props.show) { return null; }
    if (this.props.show) {
      return (
        <div id='layer3' className='layer3 animated zoomIn'>
          <div className='row spacer-1-10 spacer-p-5' />
          <div 
            className='col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3 image-holder'>
            <Dissolver display={!this.props.loading} />
            <img 
              src={this.transformURL(this.props.url)}
              className='home-image c-hand img-responsive animated fadeIn'
              onLoad={e => this.props.loaded(e)}
              onError={e => this.props.loaded(e)}
              onClick={(e) => this.props.closeModal()}
              style={{'display': this.props.loading ? 'block' : 'none'}}
            />
          </div>
        </div>)
    }
  }
}

const mapProps = ({modal}, props) => ({ ...modal });
const mapDispatch = (dispatch) => ({closeModal: () => {
    if (isMobile) {
      dispatch({ type: 'SHOW_MODAL', src: null });
    }
  }
});
export default connect(mapProps, mapDispatch)(Show_Modal);
