import React from 'react';

const Error = (props) =>{
  function mapObject(errors){
    if(!errors){
      return null;
    }
    return Object.keys(errors).map(function(key, index){
      return(<p key={`error_${key}_${index}`}>{errors[key]}</p>)
    });
  }
  return(
<div className='col-xs-12 panel text-danger text-center'>
  <div className='spacer-p-1 spacer-l-1 row' />
  <i 
    className="fa fa-exclamation-triangle fa-5x col-xs-12" 
    aria-hidden="true" />
  <h4 className='panel-header'>
    Error
  </h4>
  <p>We're sorry but we have experienced an unexpected error</p>
  {mapObject(props.errors)}
</div>
  );
}
export default Error;
