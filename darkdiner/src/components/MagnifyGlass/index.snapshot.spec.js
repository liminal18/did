import React from 'react';
import TestElement from './index.jsx';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer.create(
    <TestElement />
  ).toJSON();
  expect(tree).toMatchSnapshot();
})
