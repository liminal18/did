import React from 'react';
import { store } from 'Redux/store';
import { isMobile } from 'Services/mobile';

/***
* Magnify Glass
*/

export default class MagnifyGlass extends React.Component {
  constructor(props){
    super(props);
    this.state = store.getState();
  }
  magnify() {
    store.dispatch({ type: 'MAGNIFY' });
  }
  getClasses(mag, el) {
    if (el == 'img') {
      if (mag) {
        return 'img-responsive col-xs-10 col-md-7';
      }
      return 'img-responsive col-xs-12 col-md-9';
    }
    if (el == 'span') {
      if (mag) {
        return 'col-xs-10 mobile-font';
      }
      return null;
    }
  }
  render() {
    if (isMobile) {
      return (
        <div id="magnify" className={`c-hand no-padding spacer-l-2 col-ms-3 ${this.props.cols}`} onClick={this.magnify}>
          <div className="col-xs-12 spacer-p-1 spacer-l-1" />
          <div className="col-md-8 col-xs-12 text-center">
            <img src="images/magnifying-glass-icon.png" className={this.getClasses(this.state.magnify, 'img')} />
            <span className={this.getClasses(this.state.magnify, 'span')}>
              {this.state.magnify ? 'Back' : 'Magnify'}
            </span>
          </div>
        </div>);
    }
    return null;
  }
};
