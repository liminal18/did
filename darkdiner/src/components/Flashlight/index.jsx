import React from 'react';
import { isMobile, isAndroid, getHalf } from 'Services/mobile';

/***
* flashlight
* this is the class fro the flashligt effect
* the 3 vars below are glabals thal allow me to 
* clear the calls to window on component onMount
* using jQuery for the animation to speed it up
*/

let touchCB, mouseCB, resizeCB, touchEndCB;

export default class FlashLight extends React.PureComponent {
  constructor(props){
    super(props);
    this.state = { resize: false };
  }
  getWindowHeight(pixelRatio) {
    return window.innerHeight * pixelRatio * 3;
  }
  getWindowWidth(pixelRatio) {
    return window.innerWidth * pixelRatio * 3;
  }
  onResize() {
    this.setState({ resize: !this.state.resize });
    this.moveIt(getHalf(window.innerWidth, 2), getHalf(window.innerHeight, 2));
  }
  componentDidMount() {
    touchCB = this.touchMove.bind(this);
    mouseCB = this.mouseMove.bind(this);
    touchEndCB = (e) => this.moveIt(
      e.changedTouches[0].clientX, 
      e.changedTouches[0].clientY
    );
    resizeCB = this.onResize.bind(this);
    this.moveIt(getHalf(window.innerWidth, 2), getHalf(window.innerHeight, 2));
    if (isMobile) {
      window.addEventListener('touchmove', touchCB, true);
      window.addEventListener('touchend', touchEndCB, true);
    } else {
      window.addEventListener('mousemove', mouseCB, true);
    }
    window.addEventListener('resize', resizeCB);
    jQuery(window).ready(resizeCB);
  }
  componentWillUnmount() {
    window.removeEventListener('mousemove', mouseCB, true);
    window.removeEventListener('touchmove', touchCB, true);
    window.removeEventListener('touchend', touchEndCB, true);
    window.removeEventListener('resize', resizeCB);
  }
  moveIt(x, y) {
    const xPX = `${x - getHalf(this.getWindowWidth(window.devicePixelRatio), 2)}px`;
    const yPX = `${y - getHalf(this.getWindowHeight(window.devicePixelRatio), 2)}px`;
    if (isAndroid) {
      jQuery('#flashlight').css('transform', `translate3d(${xPX},${yPX} ,0px)`);
      jQuery('#flashlight').css('WebkitTransform', `translate3d(${xPX},${yPX} ,0px)`);
    } else {
      jQuery('#flashlight').css('transform', `translate(${xPX},${yPX})`);
      jQuery('#flashlight').css('WebkitTransform', `translate(${xPX},${yPX})`);
    }
  }
  /**
   * mouseMove - handles the mouse inputs for the flashlight on desktop
   * @param {Object} e - the mouse event
  */
  mouseMove(e) {
    e.preventDefault();
    this.moveIt(e.clientX, e.clientY);
  }
  /**
   * touchMove handles the touches for flashligt
   * @param {Object} e - the touch events
  */
  touchMove(e) {
    e.preventDefault();
    this.moveIt(e.touches[0].clientX, e.touches[0].clientY);
  }
  getBackground(coord) {
    if (!this.state.lights) {
      if (coord == 'x') {
        return `${this.state.mouse.clientX - 2500}px`;
      }
      if (coord == 'y') {
        return `${this.state.mouse.clientY - 2500}px`;
      }
      return null;
    }
  }
  render() {
    return (
      <div 
        id='flashlight' 
        className='animated fadeIn zoom'
        style={{
          width: this.getWindowWidth(window.devicePixelRatio),
          height: this.getWindowHeight(window.devicePixelRatio),
        }}
     />
    );
  }
};
