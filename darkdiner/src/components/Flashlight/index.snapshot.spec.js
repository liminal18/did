import React from 'react';
import TestElement from './index.jsx';
import renderer from 'react-test-renderer';
global.jQuery = () => ({
  css: () => {},
  ready: () => true
});

it('renders correctly', () => {
  const tree = renderer.create(
    <TestElement />
  ).toJSON();
  expect(tree).toMatchSnapshot();
})
