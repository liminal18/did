import React from 'react';
import { store } from 'Redux/store';
import { sendFBevent } from 'Services/analytics';

/***
* LightSwitch component
* @param {Bool} lights redux delivers this
*/

export default class LightSwitch extends React.PureComponent {
  constructor(props){
    super(props);
  }
  getImage(lights) {
    if (lights) {
      return 'images/lightswitch/btn-light-switch-off.png';
    }
    return 'images/lightswitch/btn-light-switch-on.png';
  }
  turnIt(e) {
    store.dispatch({ type: 'FLIP_THE_SWITCH' });
    sendFBevent('lightswitch', { value: store.getState().lights }, true);
    return true;
  }
  onOffClass(on, btn) {
    if (btn && on) {
      return 'col-xs-6 btn-did light-btn active text-center no-padding highlight on';
    }
    if (!on && !btn) {
      return 'col-xs-6 btn-did light-btn active text-center no-padding highlight on';
    }
    return 'col-xs-6 btn-did light-btn disabled text-center no-padding highlight off';
  }
  render() {
    return (
      <div className={this.props.cols}>
        <div id="light-switch" className="light-switch col-xs-12 no-padding">
          <img
            src={this.getImage(this.props.lights)}
            alt='lightswitch button'
            className="img-responsive c-hand col-xs-8 no-padding"
            onClick={() => this.turnIt()}
          />
          <div
            className={this.props.lights ?
            'col-xs-8 no-padding text-center text-white' : 'col-xs-8 no-padding text-white'}
          >
            <div className="col-xs-12 no-padding text-center btn-group btn-group-justified no-padding">
              <button
                type="button" className={this.onOffClass(this.props.lights, true)}
                onClick={_ => this.turnIt()}
              >On</button>
              <button
                type="button" className={this.onOffClass(this.props.lights, false)}
                onClick={_ => this.turnIt()}
              >Off</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
};
