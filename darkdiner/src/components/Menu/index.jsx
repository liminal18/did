import React from 'react';
import MainPage from 'Components/MainPage/';
import BackGround from 'Components/Background/';
import Show_Modal from 'Components/Show_Modal';
import { SplashPage } from 'Components/Splash';
import { isMobile } from 'Services/mobile';
import { connect } from 'react-redux';

/***
* menu component
*/

class Menu extends React.PureComponent {
  constructor(props){
    super(props);
    this.state = { img_loaded: false };
  }
  loaded(e) {
    this.setState({ img_loaded: true });
  }
  closeModal() {
   const loadedShown_notMobile = this.state.img_loaded &&
	   this.props.modal.show &&
	   !isMobile;
    if (loadedShown_notMobile) {
      this.props.closeModal();
      this.setState({ img_loaded: false });
    }
  }
  render() {
    const { display, location, section, lights } = this.props;
    return (
      <div id='stage' onClick={this.closeModal.bind(this)} className='layer1'>
        {
          display ? null : <BackGround location={location} />
        }
        <Show_Modal loading={this.state.img_loaded} loaded={this.loaded.bind(this)} /> 
        {
          display ? <SplashPage /> : <MainPage />
       }
        { this.props.children }
      </div> 
  );
  }
};

const mapProps = (state, props) => ({
 ...state
});

const mapDispatch = (dispatch) => ({
  closeModal: () => dispatch({type: 'SHOW_MODAL', url: null})
});

export default connect(mapProps, mapDispatch)(Menu);
