import React from 'react';
import { store } from 'Redux/store';

class MenuItem extends React.PureComponent {
  constructor(props){
    super(props);
    this.state = {loaded: false};
  }
  changeSection(section) {
    const waitOnLoad = Date.now() - this.props.updatedAt;
    if(waitOnLoad > 1000){
      store.dispatch({ type: 'CHANGE_SECTION', section });
    }
  }
  getItemTitle(itemName) {
    const name = itemName.toLowerCase();
    switch (name) {
      case 'media':
        return 'Gallery';
      case 'contact us':
        return 'Contact / Map';
      default:
        return itemName;
    }
  }
  render() {
    return (
      <div>
        <h4
          onClick={() => this.changeSection(this.props.item)}
          className="menu-item highlight font-Avenir c-hand text-center mobile-font col-xs-12"
        >
          {this.getItemTitle(this.props.item)}
        </h4>
        <div className='col-xs-12 spacer-l-2 spacer-p-3' />
      </div>
    );
  }
};

/** 
* menu items
*/
export default class Menu_Items extends React.PureComponent {
  constructor(props){
    super(props);
  }
  shouldComponentUpdate() {
    return false;
  }
  render() {
    const items = ['Home', 'Experience', 'Cuisine', 'Purpose', 'Events', 'Media', 'Reviews', 'Reservations', 'Contact us'];
    return (
      <span className='text-purple'>
        { items.map((name, index) => <MenuItem updatedAt={Date.now()} item={name} key={'MenuItem_' + name + '_' + index} />)}
      </span>
     ) 
  }
};

