import React from 'react';

export const Dissolver = ({display}) => {
  return(
    <p 
      id='did__dissolving' 
      className='col-xs-12 zoom bg-gradient'
      style={{'display': display ? 'block' : 'none'}}
    >
      <area className='col-xs-12 spacer-p-10 spacer-l-10' />
      <span className='col-xs-4' />
      <span className='col-xs-4 text-center'>
      </span>
      <span className='col-xs-4' />
    </p>
  );
};

const Spinner = (props) => {
  return(
    <p id='did__loading' className='col-xs-12 zoom'>
      <area className='col-xs-12 spacer-p-1 spacer-l-1' />
      <span className='col-xs-4' />
      <span className='col-xs-4 text-center'>
        <i className="fa fa-cog fa-spin fa-5x fa-fw" />
        <strong className='text-big col-xs-12'>Loading</strong>
      </span>
      <span className='col-xs-4' />
    </p>
  );
};

export default Spinner;
