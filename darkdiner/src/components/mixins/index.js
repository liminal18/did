import React from 'react';
const { isMobile } = require('../../Services/mobile');
const { store } = require('../../redux/store');
const ReactDOM = require('react-dom');
/** *
* wait on images
*/
exports.ImageWaiter = class ImageWaiter extends React.PureComponent {
  constructor(props){
    super(props);
    this.state = {loaded: 0};
  }
  loadCallback() {
    this.state.loaded++;
  }
  getClasses(l) {
    const timediff = Date.now() - this.state.start;
    if (timediff > 1000) {
      return 'text-purple scrollable text-left';
    }
    if (l >= 3 && timediff < 40) {
      return 'text-purple text-left scrollable animated slideInUp';
    }
    return 'text-purple text-left scrollable animated fadeIn';
  }
};
/**
* javascript scroll function
*/
exports.scrollable = {
  homePage: null,
  scrollMe(e) {
    if (!store.getState().lights) {
      return false;
    }
    const Page = ReactDOM.findDOMNode(this);
    Page.scrollTop += e.deltaY;
  },
  tstart(e) {
    this.lastTouchesY = null;
    this.homePage = ReactDOM.findDOMNode(this);
    this.homePage.scrollTop = this.lastPos;
  },
  lastPos: 0,
  lastTouchesY: null,
  scrollTouch(e) {
    if (!store.getState().lights) {
      return false;
    }
    if (e.touches.length >= 1 && e.touches.length < 3) {
      let scrollfactor = this.lastTouchY != null ? this.lastTouchY - e.touches[0].clientY : 0;
      if (scrollfactor < -40) {
        scrollfactor = -1;
      }
      if (scrollfactor > 80) {
        scrollfactor = 1;
      }

      this.homePage.scrollTop += scrollfactor * 2;
      this.lastPos = this.homePage.scrollTop;
      this.lastTouchY = e.touches[0].clientY;
    }
  },
  componentDidMount() {
    if (isMobile) {
      document.addEventListener('touchmove', this.scrollTouch);
      document.addEventListener('touchstart', this.tstart);
      document.addEventListener('touchend', this.tstart);
    } else {
      document.addEventListener('wheel', this.scrollMe);
    }
  },
  componentWillUnmount() {
    if (isMobile) {
      document.removeEventListener('touchmove', this.scrollTouch);
      document.removeEventListener('touchstart', this.tstart);
      document.removeEventListener('touchend', this.tstart);
    } else {
      document.removeEventListener('wheel', this.scrollMe);
    }
  },
};
