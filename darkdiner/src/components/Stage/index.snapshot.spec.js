import React from 'react';
import TestElement from './index.jsx';
import renderer from 'react-test-renderer';
import { store } from 'Redux/store';
import { Provider } from 'react-redux';

global.jQuery = () => ({
  css: () => {},
  ready: () => true
});


it('renders correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <TestElement store={store} />
    </Provider>
  ).toJSON();
  expect(tree).toMatchSnapshot();
})
