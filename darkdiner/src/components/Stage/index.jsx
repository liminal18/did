import React from 'react';
import LightSwitch from 'Components/LightSwitch/';
import Menu from 'Components/Menu/';
import FlashLight from 'Components/Flashlight/';
import { SplashMenu } from 'Components/Splash/';
import { isMobile, isAndroid } from 'Services/mobile';
import { connect } from 'react-redux';

/***
* stage component
* this is the root component which manages both the splash
* and the curtian and also main
*/

class Stage extends React.PureComponent {
  constructor(props){
    super(props);
  }
  componentDidMount() {
    window.addEventListener('resize', this.props.onResize);
  }
  componentWillUnmount() {
    window.removeEventListener('resize', this.props.onResize);
  }
  
  getLayer(lights) {
    if (lights) {
      return 'animated fadeIn';
    }

    return 'layer3 animated fadeIn';
  }
  getLightSwitch(lights) {
    if (lights) {
      return null;
    }
    if (!lights) {
      const androidStyle = 'col-xs-2 no-padding';
      const otherStyle = 'col-md-1 col-xs-4 no-padding';
      return(
        <LightSwitch
          resize={this.props.resize}
          lights={lights}
          cols={isAndroid ? androidStyle : otherStyle }
        />
      )
   }
  }
  render() {
    const {lights, display} = this.props;
    return(
      <Menu>
        <div id='curtain' className={this.getLayer(lights)}>
          {lights ? null : <FlashLight />}
          {!display ? this.getLightSwitch(lights) : <SplashMenu />}
        </div>
      </Menu>
    )
  }
}

const mapProps = (state, props) => {
 return {
    ...state
  }
};

const mapDispatch = (dispatch) => ({
  onResize: () => dispatch({ type: 'RESIZE' })
  });

export default connect(mapProps, mapDispatch)(Stage);
