import React from 'react';
import { store } from 'Redux/store';
/***
* Modal_Image
*/
export default class Modal_Image extends React.PureComponent {
  constructor(props){
    super(props);
  }
  showModal(url) {
    store.dispatch({ type: 'SHOW_MODAL', src: url });
  }
  transformURL(url) {
    return `thumbNails/${url}`;
  }
  shouldComponentUpdate(next_props, _) {
    if (next_props.url != this.props.url) {
      return true;
    }
    if (next_props.size != this.props.size) {
      return true;
    }
    return false;
  }
  render() {
    return (
      <div className={`image-holder ${this.props.size}`}>
        <img 
          src={this.transformURL(this.props.url)} 
          className='home-image c-hand img-responsive'
          alt={this.props.alt || 'DiD Experience'}
          onClick={() => this.showModal(this.props.url)}
        />
      </div>
    );
  }
};
