import React from 'react';
import {SplashPage, SplashMenu } from './index.jsx';
import renderer from 'react-test-renderer';
import { store } from 'Redux/store';

it('renders correctly', () => {
  const tree = renderer.create(
    <SplashPage />
  ).toJSON();
  expect(tree).toMatchSnapshot();
})


it('renders correctly', () => {
  const tree = renderer.create(
    <SplashMenu store={store} />
  ).toJSON();
  expect(tree).toMatchSnapshot();
})
