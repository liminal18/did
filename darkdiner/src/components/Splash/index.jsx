import React from 'react';
import ReactDOM from 'react-dom';
const { sendGaEvent, sendFBevent } = require('Services/analytics');
import Tapable from 'Components/Tapable/';
import { connect } from 'react-redux';

export class SplashPage extends React.PureComponent {
  constructor(props){
    super(props);
  }
  componentDidMount() {
    sendFBevent('ViewContent', { content_name: 'splash' });
  }
  render() {
    return(
      <div className='col-xs-12' style={{backgroundColor: 'rgb(122, 23, 210)' }}>
        <div id='splash-top' className='splash row'>
          <div id='splash-top-left' className='col-xs-6' />
          <div id='splash-top-right' className='col-xs-3 col-ms-6'>
            <div className='row spacer-p-3 spacer-l-5' />
            <img src='images/splash/glass.png' className='img-response center-block' />
          </div>
        </div>
        <div id='splash-bottom' className='splash row'>
          <div id='splash-bottom-left' className='col-xs-6'>
            <div className='row spacer-l-4' />
            <div className='col-xs-2 col-md-3' />
            <img src='images/splash/fork.png' className='img-responsive center-block' />
          </div>
          <div id='splash-bottom-right' className='col-xs-6'>
            <div className='row spacer-l-1' />
            <img src='images/splash/spoon-knife.png' className='img-responsive center-block' />
          </div>
        </div>
      </div>
    ) 
  }
}

class SM extends React.PureComponent {
  constructor(props){
    super(props);
  }  
  componentWillUnmount() {
    const stuff = jQuery(ReactDOM.findDOMNode(this));
    stuff.empty();
  }
  render() {
    let h2Class = 'text-white tapable font-Avenir';
    h2Class += ' font-38-portrait tapable-item c-hand';
    return(
      <div className='col-xs-12'>
        <div id='splash-phone-space' className='row spacer-p-20' />
        <div id='sp-menu-spacer' className='row col-xs-12 spacer-l-5' />
        <div id='sp-menu-top' className='row'>
          <div id='sp-menu-top-left' className='col-xs-4' />
          <div id='sp-menu-top-middle' className='col-xs-4'>
            <div className='col-md-8 col-md-offset-2'>
              <img src='images/logo_splash.png' 
                className='img-responsive center-block' />
            </div>
          </div>
          <div id='sp-menu-top-right' className='col-xs-4' />
        </div>
        <div id='sp-menu-spacer' className='row col-xs-12 spacer-l-5' />
        <div id='sp-menu-middle' className='row'>
          <div className='col-xs-2' />
          <div id='middle-column' className='col-xs-8'>
            <div className='col-xs-12 spacer-l-3 spacer-p-4' />
            <div className='col-xs-12 col-ms-5 col-md-5 text-center SplashTap'>
              <Tapable action={() => this.props.goToLocation(1)}>
                <h2 role='button' tabIndex='-1' className={h2Class}> 
                 Bangkok 
                </h2>
              </Tapable>
            </div>
            <div className='col-xs-12 col-ms-2 col-md-2 spacer-p-4' />
            <div className='col-xs-12 col-ms-5 col-md-5 text-center SplashTap'>
              <Tapable action={() => this.props.goToLocation(2)}>
                <h2 role='button' tabIndex='-1' className={h2Class}>
                  Phnom Penh
                </h2>
              </Tapable>
            </div>
          </div>
          <div className='col-xs-2' />
        </div>
        <div id='sp-menu-bottom' className='row' />
      </div>
    )
  }
}

const mapProps = (state, props) => props;
const mapDispatch = (dispatch) => ({
    goToLocation: (loc) => {
      dispatch({ type: 'GO_TO_LOCATION', loc });
      sendFBevent('changedlocation',
          { content_name: loc === 1 ?
            'Bangkok' : 'PhnomPenh' }, true);
  }});
export const SplashMenu = connect(mapProps, mapDispatch)(SM);
