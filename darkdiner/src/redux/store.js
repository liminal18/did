const Redux = require('redux');
const State = require('./state');

function reducer(state, action) {
  switch (action.type) {
    case 'GO_TO_LOCATION':
      state.display = false;
      state.lights = true;
      state.location = action.loc;
      return { ...state };
    case 'CHANGE_SECTION':
      state.section = action.section;
      return { ...state };
    case 'FLIP_THE_SWITCH':
      state.lights = !state.lights;
      return { ...state };
    case 'RESIZE':
      state.resize = !state.resize;
      return { ...state };
    case 'SHOW_MODAL':
      state.modal.show = !state.modal.show;
      state.modal.url = action.src;
      return { ...state };
    default:
      return state;
  }
}

const store = Redux.createStore(reducer, State);
exports.changeSection = function (section) {
  if (section) {
    store.dispatch(
      { type: 'CHANGE_SECTION', section });
  }
};
exports.store = store;
