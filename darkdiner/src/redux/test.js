import { store } from './store.js';

const defaultState = () => ({
  section: 'Home', 
  display: true, 
  menu: 0, 
  lights: false, 
  location: 1, 
  resize: null,  
  modal: {
   show: false, 
   url: null
  }
});

describe('redux should', () => {
  const newState = defaultState();
  it('return home', () => {
    store.dispatch({type:'GO_TO_LOCATION', loc: 1});
    newState.location = 1;
    newState.display = false;
    newState.lights = true;
    expect(store.getState()).toMatchObject(newState);
  })

  it('change section', () => {
    store.dispatch({type: 'CHANGE_SECTION', section: 'test'});
    newState.section = 'test';
    expect(store.getState()).toMatchObject(newState);
  })

})
